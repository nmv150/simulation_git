import numpy as np
import emcee

def log_prob(x, mu, cov):
    
    
    if ((x>1).any() | (x<0).any()):
        return -np.inf
    
    else:
        diff = x - mu
        return -0.5*np.dot(diff, np.linalg.solve(cov,diff))

ndim = 2

np.random.seed(42)
means = np.array([0.5]*ndim)

cov = 0.5 - np.random.rand(ndim ** 2).reshape((ndim, ndim))
cov = np.triu(cov)
cov += cov.T - np.diag(cov.diagonal())
cov = np.eye(2)*0.5

nwalkers = 20
p0 = np.random.rand(nwalkers, ndim)

sampler = emcee.EnsembleSampler(nwalkers, ndim, log_prob, args=[means, cov])

state = sampler.run_mcmc(p0, 100000)
sampler.reset()

sampler.run_mcmc(state, 100000)

import matplotlib.pyplot as plt

samples = sampler.get_chain(flat=True)
plt.hist(samples[:, 0], 100, color="k", histtype="step")
plt.xlabel(r"$\theta_1$")
plt.ylabel(r"$p(\theta_1)$")
plt.gca().set_yticks([]);

print(samples.mean(axis=0))