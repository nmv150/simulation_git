import copy
import scipy.integrate
import scipy.stats
import numpy as np
import pandas as pd
import scipy.optimize
import value_functions
import dill
import matplotlib.pyplot as plt
from scipy.stats import norm, multivariate_normal
#import line_profiler
import quadpy

class Meet:
    def __init__(self, meet_params):
        (self._d, self._a) = (meet_params['d'],meet_params['a'])

    def theta(self,state,shares):  
        self.draws = self._d[0][0] + self._d[0][1]*state[0] + self._d[0][2]*state[2]
        return self._d[1:4]*state[1][0, :]/(self.draws*shares)
   
    def q_x(self,state,shares): 
        return scipy.stats.expon.cdf(self.theta(state,shares),scale=1/self._a)

    def q_y(self,state,shares): 
        return (1/(self._a*self.theta(state,shares)))*scipy.stats.expon.cdf(self.theta(state,shares),scale=1/self._a)

class Demand:
    def __init__(self, match_params, meet_params, demand_params, surplus, n_total):
        (self._gamma, self._c, self._e, self._p_tau) = (demand_params['gamma'], demand_params['c'], demand_params['e'], demand_params['p_tau']) 
        
        self.match_params = match_params
        
        #self.match_params['low1'] = self.match_params['low1'] + self._v[1]
        
        self.meet_params = meet_params
        self.demand_params = demand_params
        
        denom = norm.cdf(1.6,demand_params['mu'],demand_params['sigma'])-norm.cdf(0,demand_params['mu'],demand_params['sigma'])
        self.f = lambda x: self.normal_pdf(x,demand_params['mu'],demand_params['sigma'])/denom
        
        self.mri_max = np.array([1.6,1.6,1.6])
        self.mri_min = np.array([0,0,0])
        self.value_max = np.array([0.5,0.5,0.5])
        self.value_min = np.array([0,0,0])
        
        self.state = [1,np.ones((3,3))*(1/3),0] 
        
        self.shares = np.array([1/3,1/3,1/3])
        self.accept_y = np.array([1/3,1/3,1/3])
        self.expect_y = np.array([1/3,1/3,1/3])
        self.match_y = np.array([1/3,1/3,1/3])

        self.meet = Meet(meet_params)
        self.q_x = self.meet.q_x(self.state,self.shares) 
        
        self.match_params = match_params
        
        self._rho = [1,0]
        self.beta = 0.99
        self.scheme = quadpy.line_segment.gauss_patterson(4)
        self.surplus = surplus
        
    def normal_pdf(self,x,mu,sigma_sq):
        constant = 1/(np.sqrt(2*np.pi*sigma_sq))
        exponential = np.exp((-(x-mu)**2/(2*sigma_sq)))
        output = (constant*exponential).reshape((-1,1))
        
        return output
    
    def integrate(self, integrand, y_min, y_max,compute_shares=False,state_empirical=None):
        output = np.zeros(3)
        for tau in [2,3,4]:
            for y in [0,1,2]:
                output_tau = self._p_tau[tau]*self.scheme.integrate(lambda x: integrand(x,tau,y,compute_shares,state_empirical),
                                                                      [y_min[tau][y], y_max[tau][y]])
                output[y] += output_tau

        return output
    
    def integrate_dbl(self, integrand_dbl):
        output = []
        for y in [0,1,2]:
            output.append( scipy.integrate.dblquad( lambda x1,x2,y: integrand_dbl([x2,x1])[y],
                                                  a=self.mri_min[y],
                                                  b=self.mri_max[y],
                                                  gfun = lambda v: self.value_min[y],
                                                  hfun = lambda v: self.value_max[y],
                                                  args=[y])[0] )  
        return np.array(output)
    
    #@profile
    def weights(self,x,tau,state_empirical): 
        
        ev = self.surplus.surplus(state_empirical,x)[tau]*self.q_x.reshape((-1,1))
        
        num = self.state[1][0,:].reshape((-1,1))*np.exp(self._gamma.reshape((-1,1))*ev)
        
        num_sum = num.sum(axis=1).reshape((-1,1))
        
        mnl = num/num_sum
        
        entry = np.exp((mnl*ev).sum()-self._c)/(1+np.exp((mnl*ev).sum()-self._c))     
        
        weight = mnl*entry

        return weight

    def target_pdf_num(self,x,tau,y,compute_shares,state_empirical): 
        
        if compute_shares == True:
            if y == 0:
                weights = self.weights(x,tau,state_empirical)
                f = self.f(x).T[0]
                
                self.saved_target_output = ( weights*f )
            
            return self.saved_target_output[y]
        
        else:
            weights = self.weights(x,tau,state_empirical)
            f = self.f(x).T[0]
            target_output = ( weights*f )  
            
            return target_output[y]
    
    def compute_target_pdf(self,state_empirical):   
        
        y_min = {2: [0,0,0],
                 3: [0,0,0],
                 4: [0,0,0]}
        
        y_max = {2: [1.6,1.6,1.6],
                 3: [1.6,1.6,1.6],
                 4: [1.6,1.6,1.6]}
        i = 0
        while(i < 100):        
            self.q_x = self.meet.q_x(self.state,self.shares) 
            s_1 = self.integrate(self.target_pdf_num,y_min=y_min,y_max=y_max,compute_shares=True,state_empirical=state_empirical) 
            if (np.max(np.abs(s_1 - self.shares)) < 0.0001): break
            self.shares = s_1
            i += 1     
        else: 
            print("Error: Target PDF failed to converge")
            print(self.match_params)
            print(self.meet_params)
            print(self.demand_params)
        
        return lambda x,tau,y,compute_shares,state_empirical: self.target_pdf_num(x,tau,y,compute_shares,state_empirical)/self.shares[y]
    
    def compute_match(self,state_empirical):
        target_pdf = self.compute_target_pdf(state_empirical) 
        
        accept_y = self.integrate(target_pdf,y_min=self.y_min,y_max=self.y_max,state_empirical=state_empirical)
        accept_y = np.array([ min(accept_y[0],1), min(accept_y[1],1), min(accept_y[2],1) ])
        
        expect_y = self.integrate(lambda x,tau,y,compute_shares,state_empirical: x*target_pdf(x,tau,y,compute_shares,state_empirical),
                                  y_min=self.y_min,
                                  y_max=self.y_max,
                                  state_empirical=state_empirical)/accept_y       
        match_y = self.meet.q_y(self.state,self.shares)*accept_y
        
        #AV VALUE COMPUTED AT TAU = 3
        average_match_value_y = np.array([0,0,0])
        
        #print("V",self._v)
        #print("match",self.match_params)
        #print("expect_y",expect_y)
        
        average_match_value_y = []
        for i, spec in enumerate(['low','mid','high']):
            average_match_value_y.append(np.sqrt( (self.match_params['gas0']+self.match_params['gas1']*expect_y[i]) ) + (self.match_params[spec+'0'] + self.match_params[spec+'1']*expect_y[i]) )
        
        average_match_value_y = np.array(average_match_value_y)
        
        #print("av match",average_match_value_y)
        
            #average_match_value_y = - self._cost(expect_y,3,spec) + self._value_gas(expect_y,3) 
        
        return(accept_y,expect_y,match_y,average_match_value_y)
        
    def update_state(self,state_empirical=None):
        for y in [0,1,2]:
            T = np.array([[1-self.match_y[y], 0, self.match_y[y]],
                          [1-self._e, 0, self._e],
                          [0, 1, 0]])
            self.state[1][:,y] = np.matmul(self.state[1][:,y],T)
        
        # use the empirical state?
        if state_empirical is not None:
            self.surplus_state = state_empirical
            self.state[0] = state_empirical[0]
        
        self.state[2] += 1

    def g_1(self,g):
            return 0.64 + 0.89*g
    
    def g(self,g,tau):

        if tau == 2:
            return 0.64 + 0.89*(0.64 + 0.89*g)
        
        if tau == 3:
            return 0.64 + 0.89*(0.64 + 0.89*(0.64 + 0.89*g))
        
        if tau == 4:
            return 0.64 + 0.89*(0.64 + 0.89*(0.64 + 0.89*(0.64 + 0.89*g)))
        
    def _value_gas(self,x,tau):
        beta_tau = self.beta**tau 
        g_tau = self.g(self.state[0],tau)

        x_value = self._v[0] + self._v[1]*x
        
        value = beta_tau*( g_tau*self._rho[1]*x_value 
                               + self._rho[0]*x_value )
        
        #CHANGE BELOW
        value = np.sqrt(x_value)
        
        return value
    
    def _cost(self,x,tau,spec):
        if tau == 2:
            match_periods = 1 + self.beta
            
        if tau == 3:
            match_periods = 1 + self.beta + self.beta**2
            
        if tau == 4:
            match_periods = 1 + self.beta + self.beta**2 + self.beta**3

        # CHANGE BELOW
        match_periods = 1

        cost = match_periods*( self.match_params[spec+'1']*x + self.match_params[spec+'0'] )
        return cost
            
    def one_period(self,state_empirical):      
        self.update_state(state_empirical)   
        
        #print(state_empirical)
        
        # 1. UPDATE CUTOFFS
        self.y_max = dict()
        self.y_min = dict()
        
        for tau in [2,3,4]: 
            self.y_min[tau] = [0,
                               max(0,self.surplus.cutoff(state_empirical,tau,spec='mid')),
                               self.surplus.cutoff(state_empirical,tau,spec='high') ]
            self.y_max[tau] = [self.surplus.cutoff(state_empirical,tau,spec='low'),1.6,1.6]
        
        # 2. FIND THE MATCH    
        (self.accept_y,self.expect_y,self.match_y,self.average_match_value) = self.compute_match(state_empirical)
        
        #print( self.surplus.cutoff(state_empirical,tau,spec='high') )
        
        #print("LOW CUT", self.y_min[3])
        #print("HIGH CUT", self.y_max[3])
        
    def burn_in(self,state_empirical):
        print("Doing burn-in")
        j = 0
        while(j<1000):
            s_0 = copy.deepcopy(self.state)
            self.one_period(state_empirical)
            self.state[2] = 0
            eps = np.max(np.abs(s_0[1] - self.state[1]))
            #print(str(j) + ': ' + str(eps))
            j += 1
            if (eps < 0.001): 
                print("Burn-in successful!")
                break
            
        else: 
            print("Error: Burn-in failed to converge")    

class Estimation:
    def __init__(self):  
        self.data_contracts = 1 #Import contract data etc...
        self.n_total = np.array([20,20,20])
        self.eta = 0.5
        self.beta = 0.99
        #self.moments_data = pd.DataFrame([0.78,0.83,0.78+0.065,0.71,0.83,0.91], 
         #                                index = ['E_y_l_bust',
          #                                        'E_y_m_bust',
           #                                       'E_y_h_bust',
            #                                      'E_y_l_boom',
             #                                     'E_y_m_boom',
              #                                    'E_y_h_boom'])
    
        # NOTE: COVAR MOMENTS BELOW ARE SET TO HIT PARTICULAR VALS
        self.moments_data = pd.DataFrame([0.78,0.83,0.78+0.065,-0.07,0,0.065,0.63,0.77,0.93,0.2,0.11,0.09,0,0,0,0,0,0,0,0,0], 
                                         index = ['E_y_l_bust',
                                                  'E_y_m_bust',
                                                  'E_y_h_bust',
                                                  'E_y_l_diff',
                                                  'E_y_m_diff',
                                                  'E_y_h_diff',
                                                  'Util_l_mean',
                                                  'Util_m_mean',
                                                  'Util_h_mean',
                                                  'Util_l_covar',
                                                  'Util_m_covar',
                                                  'Util_h_covar',
                                                  'p_intercept_low',
                                                  'p_mri_low',
                                                  'p_intercept_mid',
                                                  'p_mri_mid',
                                                  'p_intercept_high',
                                                  'p_mri_high',
                                                  'delta_low',
                                                  'delta_mid',
                                                  'delta_high'])
   
    def _construct_surplus(self,demand_params):
        
        def surplus(x,state,spec = None):
            
            state_input = state # np.array([state[0], *state[1][0, :]])
            
            # Note: inputs are reversed in scipy.integrate...
            x_input = {'mri': x,
                       'value': 0.0002*x + 0.0005,
                       'tau': 3,
                       'waterd': 0}
                    
            if spec is None:
                
                surplus_args = {'s': state_input,
                                'x': x_input,
                                'v_evol': 'use_empirical_states',
                                'points_evol': 'use_empirical_states'}
                
                output =  np.array([ self.model.v[spec].surplus(**surplus_args) for spec in ['low','mid','high'] ])
          
                return output
            
        return surplus
    
    def _monthly_compute_exponential(self,Q,t):
        yearly_decline = 0.3
        D = (1+ yearly_decline)**(1/12)-1
        
        q = Q*np.exp(-1*D*t)
        
        return q

    def _compute_lifetime(self,g,Q):
        total = 0
        beta = 0.99
        
        for t in range(0,240):
            total += g*beta*self._monthly_compute_exponential(Q,t)
            g = 0.64 + 0.89*g
            
        return(total)
    
            
    def plot_v(self,use_saved_model = False):
        if (use_saved_model == True):
            print('Loading saved v')
            f = open('model_dill', 'rb')
            self.model = dill.load(f)
            f.close()
            
        data = self.model.data_state
        for spec in ['low','mid','high']:
            self.model.v[spec].plot_value_function(data[['g','n_l','n_m','n_h']])   
    
    def simulate(self, match_params, meet_params, demand_params, model, verbose = False,set_zero=False):
        # SETUP PARAMETERS #
        
        print("SET ZERO",set_zero)
        
        # CHANGE TO FIT = TRUE WHEN ACTUALLY ESTIMATING THE MODEL!!
        
        self.model.fit_match_params(match_params)
        
        if set_zero == True:
            surplus = value_functions.Surplus_zero(self.model)
        else:
            surplus = value_functions.Surplus(self.model)
        
        state_init = self.model.data_state.loc[ pd.to_datetime('1/1/2000'),['g','n_l','n_m','n_h']]
        state_init_tuple = (state_init['g'],
                            state_init['n_l'],
                            state_init['n_m'],
                            state_init['n_h'])

        self.demand = Demand(match_params, meet_params, demand_params, surplus, self.n_total)

        self.demand.burn_in(state_init_tuple)
    
        simulation_list = []
        
        for i in self.model.data_state[['g','n_l','n_m','n_h']].itertuples(): 
            k = i.Index          
            state_tuple = (i.g,i.n_l,i.n_m,i.n_h)
            
            self.demand.one_period(state_tuple)
            
            simulation_list.append([k,
                                    state_tuple[0],
                                    *self.demand.shares,
                                    *self.demand.meet.q_y(self.demand.state,self.demand.shares),
                                    *(1 - self.demand.state[1][0,:]).round(2),
                                    *self.demand.match_y,
                                    *self.demand.expect_y,
                                    self.demand.meet.draws,
                                    *self.demand.average_match_value].copy())
            
            verbose = False
            if verbose == True:
                print('Date: ' + str(k.year) + '-' + str(k.month),
                      'Gas price: ' + str(state_tuple[0]),
                      'Shares: ' + str(self.demand.shares.round(5)),
                      'Theta: ' + str(self.demand.meet.theta(self.demand.state,self.demand.shares).round(2)),
                      'q_y: ' + str(self.demand.meet.q_y(self.demand.state,self.demand.shares).round(2)),
                      'util: ' + str((1 - self.demand.state[1][0,:]).round(2)),
                      'match_y: ' + str(self.demand.match_y.round(2)),
                      'accept_y: ' + str(self.demand.accept_y.round(2)),
                      'E_y: ' + str(self.demand.expect_y.round(2)),
                      'Draws: ' + str(self.demand.meet.draws))
        
        simulation_df = pd.DataFrame(simulation_list, 
                                     columns = ['Date',
                                                'gas_price',
                                                'Shares_l','Shares_m','Shares_h',
                                                'Util_l','Util_m','Util_h',
                                                'Q_y_l','Q_y_m','Q_y_h',
                                                'Match_y_l','Match_y_m','Match_y_h',
                                                'E_y_l','E_y_m','E_y_h',
                                                'Draws',
                                                'E_match_value_l','E_match_value_m','E_match_value_h'])
    
        simulation_df['boom'] = (simulation_df['gas_price'] > 5.41)
    
        return simulation_df
    
    def param_names_wrapper(self,x):
        # converts params dictionary to names etc
        match_params = {'delta': x[0],
                         'low0': x[1],
                         'low1': x[2],
                         'mid0': x[3],
                         'mid1': x[4], 
                         'high0': x[5],
                         'high1': x[6],
                         'gas0': x[19],
                         'gas1': x[20]}
        
        meet_params = {'d': [ [x[7],x[8],x[9]],
                              self.n_total[0],
                              self.n_total[1],
                              self.n_total[2]],
                       'a': np.array([x[10],x[11],x[12]]) }
        
        demand_params = {'gamma': np.array([x[13],x[14],x[15]]),
                         'c': x[16],
                         'mu': np.array([x[17]]), #,x[12],x[13],x[14]]),
                         'sigma': np.array(x[18]), #,x[16],x[17],x[18]]),
                         'e': self.eta,
                         'p_tau': {2: 0, 3: 1, 4: 0} }

        return (match_params, meet_params, demand_params)
    
    def param_init_wrapper(self,match_params_init, meet_params_init, demand_params_init):
        # converts params dictionary to names etc
        
        x = [match_params_init['delta'],
             match_params_init['low0'],
             match_params_init['low1'],
             match_params_init['mid0'],
             match_params_init['mid1'],
             match_params_init['high0'],
             match_params_init['high1'],
             meet_params_init['d'][0],
             meet_params_init['d'][1],
             meet_params_init['d'][2],
             meet_params_init['a'][0],
             meet_params_init['a'][1],
             meet_params_init['a'][2],
             demand_params_init['gamma'][0],
             demand_params_init['gamma'][1],
             demand_params_init['gamma'][2],
             demand_params_init['c'],
             *demand_params_init['mu'],
             *demand_params_init['sigma'],
             match_params_init['gas0'],
             match_params_init['gas1']]
    
        x_bounds = [(0.1,0.9),  # delta
                    (-1,1),      # low0 
                    (-1,1),      # low1
                    (0,1.6),      # mid0 
                    (0,1.6),      # mid1
                    (-1,1),      # high0
                    (-1,1),      # high1
                    (5,100),     # d0
                    (5,100),     # d1
                    (-0.4,0),     # d2
                    (0.01,5),    # a0
                    (0.01,5),    # a1
                    (0.01,5),    # a2
                    (0,10),    # gamma1
                    (0,10),    # gamma2
                    (0,10),    # gamma3
                    (0,1),      # c
                    (0.5,1.2),  # mu
                    (0.7,2),  # sigma
                    (0,0.001),    # v0
                    (0,0.001)]      # v1
        
        print("LENGTH OF X: ", len(x))
        print(x)
        print("LENGTH OF BOUNDS: ", len(x_bounds))
        
        return x, x_bounds
    
    def make_moments(self,simulation_df):
        moments_match_boom = pd.DataFrame(simulation_df.loc[simulation_df['boom'] == True, ['E_y_l','E_y_m','E_y_h']].mean())
        moments_match_bust = pd.DataFrame(simulation_df.loc[simulation_df['boom'] == False, ['E_y_l','E_y_m','E_y_h']].mean())
        moments_match_diff = moments_match_boom - moments_match_bust
         
        moments_match_boom = moments_match_boom.set_index(pd.Index(['E_y_l_boom','E_y_m_boom','E_y_h_boom']))        
        moments_match_bust = moments_match_bust.set_index(pd.Index(['E_y_l_bust','E_y_m_bust','E_y_h_bust']))      
        moments_match_diff = moments_match_diff.set_index(pd.Index(['E_y_l_diff','E_y_m_diff','E_y_h_diff'])) 
        moments_util_mean = simulation_df[['Util_l','Util_m','Util_h']].mean()   
        moments_util_mean = moments_util_mean.add_suffix('_mean')
        moments_util_var = simulation_df[['Util_l','Util_m','Util_h']].var()
        moments_util_var = moments_util_var.add_suffix('_var')

        moments_util_covar_l = simulation_df[['Util_l','gas_price']].cov().loc['Util_l','gas_price']
        moments_util_covar_m = simulation_df[['Util_m','gas_price']].cov().loc['Util_m','gas_price']
        moments_util_covar_h = simulation_df[['Util_h','gas_price']].cov().loc['Util_h','gas_price']
              
        moments = pd.concat([moments_match_bust,
                          moments_match_diff,
                          moments_util_mean])
        
        moments.loc['Util_l_covar'] = moments_util_covar_l
        moments.loc['Util_m_covar'] = moments_util_covar_m
        moments.loc['Util_h_covar'] = moments_util_covar_h
        
        for i in ['l','m','h']:
            plt.plot(simulation_df['Util_' + i])
        plt.show()
        
        # MAKE THE PRICE MOMENTS
        
        # 1. FIND THE PRICE DIFFERENCES
        
        residuals = dict()
        for spec in ['low','mid','high']:
            data_select = self.model.data_contracts_state[self.model.data_contracts_state['rig_spec'] == spec + '-spec']
            data_select = data_select.dropna(subset = ['dayrate'])
            residuals[spec] = self.model.v[spec].compute_residuals(data_select)
            moments.loc['p_intercept_' + spec] = residuals[spec].mean()
            moments.loc['p_mri_' + spec] = (residuals[spec]*data_select['mri']).mean()  
            moments.loc['delta_' + spec] = (self.model.v[spec].fit_surplus_diff(data_select)*residuals[spec]).mean()
        
        return moments
    
    def GMM(self,moments,compute_weight = True):
        print("MOMENTS",moments)
        #print("MOMENTS_DATA",self.moments_data)
        #print("MOMENTS MINUS", moments - self.moments_data)
        
        diff = np.array(moments - self.moments_data).T[0]
        
        # SAVE THE DIFF FOR OPTIMAL GMM
        if compute_weight == True:  
            self.weight_matrix = np.multiply( diff.reshape((-1,1)), diff)
            SIGMA = np.eye(len(diff))*np.array([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
            
        if compute_weight == False:
            SIGMA = self.weight_matrix
        
        result = diff.T @ SIGMA @ diff.T
        return result
    
    def simulate_wrapper(self,x,moments_data,model,compute_weight,verbose = False,set_zero=False):
        # 1. convert the optimization input to param dictionary
        
        print("RAW",x)
        print(set_zero)
        
        (match_params, meet_params, demand_params) = self.param_names_wrapper(x)   
        #if verbose == True:
        
        print(match_params)
        print(meet_params)
        print(demand_params)
        
        # 2. do the simulation
        simulation_df = self.simulate(match_params, meet_params, demand_params, model, verbose, set_zero)    
        moments = self.make_moments(simulation_df) 
        result = self.GMM(moments,compute_weight)
        
        #print(moments)
        print(result)
        
        welfare = dict()
        for spec in ['l','m','h']:
            simulation_df['matches_' + spec] = simulation_df['Draws']*simulation_df['Shares_' + spec]*simulation_df['Q_y_' + spec]
            simulation_df['total_target_' + spec] = simulation_df['Draws']*simulation_df['Shares_' + spec]
            simulation_df['welfare_' + spec] = simulation_df['E_match_value_' + spec]*simulation_df['matches_' + spec] - simulation_df['total_target_' + spec]*demand_params['c']
            
            welfare[spec] = simulation_df['welfare_' + spec].sum() 
            print(spec,welfare[spec])
        
        #for spec in ['l','m','h']:
        #    plt.plot(simulation_df['welfare_' + spec])
        #plt.show()
        
        #print(simulation_df.columns.values)
        #print(simulation_df[['Match_y_l','Match_y_m','Match_y_h','Shares_l','Shares_m','Shares_h']])
        
        print("WELFARE",welfare['l']+welfare['m']+welfare['h'])
        print("OBJECTIVE",result)
          
        return result
            
    def estimate(self,match_params_init = None, meet_params_init = None, demand_params_init = None, use_saved_model = False, save = True, do_sim = True,set_zero=False):
        
        ################################
        # 1. SET UP INITIAL PARAMETERS #
        ################################
        if (match_params_init == None):
            match_params_init = { 'delta': 0.1257,
                                 'low0': 0.1865,
                                 'low1': -0.1136,
                                 'mid0': 0.1309,
                                 'mid1': 0.0186,                                 
                                 'high0': 0.0543,
                                 'high1': 0.145,
                                 'gas0': 0,
                                 'gas1': 0 }
            
            match_params_init['eta'] = self.eta 

        if (meet_params_init == None):
            meet_params_init = {'d': [5.0949,7.455,-0.16847],
                                'a': np.array([0.0979,0.061,0.0256]) }
            
        if (demand_params_init == None):
            demand_params_init = {'gamma': np.array([3,3,3]),
                                  'c': 0.0835,
                                  'mu': [1.00], 
                                  'sigma': [1.045],
                                  'p_tau': {2: 0, 3: 1, 4: 0} }
            
        x_init, x_bounds = self.param_init_wrapper(match_params_init, 
                                                   meet_params_init, 
                                                   demand_params_init)
        
        #############################
        # 2. SET UP VALUE FUNCTIONS #
        #############################
        
        if (use_saved_model == False):
            print('Setting up the inital v')
            self.model = value_functions.Model(self.eta)
            self.model.fit_v(match_params_init, fit=True)
            
            if (save == True):
                print('Saving v')
                f = open('model_dill', 'wb') 
                dill.dump(self.model,f)
                f.close()
            
        if (use_saved_model == True):
            print("SAVED")
            print('Loading saved v')
            f = open('model_dill', 'rb')
            self.model = dill.load(f)
            f.close()

        # 2. Set up moments from data

        moments_data = 1
        
        # 3. Do the optimization

        # LOCAL OPTIMIZATION:
        
        surplus = value_functions.Surplus(self.model)
        
        #self.simulate_wrapper(x_init,moments_data,surplus,False)
        
        if do_sim == True:
            
            # 1. DO FIRST STEP
            results = scipy.optimize.minimize(self.simulate_wrapper, 
                                              x_init,
                                              bounds = x_bounds,
                                              method= 'SLSQP',
                                              #tol = 0.0000000001,
                                              #options = {'maxiter':1},
                                              args = (moments_data,surplus,True,True) )
            
            print(results)
            
            # 2. DO SECOND STEP
            results = scipy.optimize.minimize(self.simulate_wrapper, 
                                              x_init,
                                              bounds = x_bounds,
                                              method= 'SLSQP',
                                              #tol = 0.0000000001,
                                              #options = {'maxiter':1},
                                              args = (moments_data,surplus,False,True) )
            
            print(results)
            
        else:
            print("SIM WRAPPER")
            self.simulate_wrapper(x_init,moments_data,self.model,True,set_zero)
        
        # GLOBAL OPTIMIZATION:
        #results = scipy.optimize.shgo(self.simulate_wrapper, 
         #                               bounds = x_bounds,
          #                              args = (moments_data,self.model,True) )

        
class Counterfactuals:
    def __init__(self):
        self.model = Estimation()
        
    def do_no_sorting(self):
        self.model.estimate(use_saved_model = True, do_sim = False, set_zero = False)
        self.model.estimate(use_saved_model = True, do_sim = False, set_zero = True)
        
        
        
        
        
        
        
        