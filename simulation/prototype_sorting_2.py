import numpy as np
import scipy.optimize
from scipy.stats import truncnorm
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
import copy

def cutoff(c,rho,p_min):    
    num = p_min - (rho[0]-c[0])
    denom = (rho[1]-c[1])
    cutoff = max(0,num/denom)
    cutoff = min(1.6,cutoff)
    return cutoff
    
def E_normal(cutoff,spec):

    if spec == 'low':
        return truncnorm.mean(0, cutoff_normal, loc=mu[0], scale=sigma[0])
    
    if spec == 'high':
        return truncnorm.mean(cutoff_normal, 1.6, loc=mu[0], scale=sigma[0])
    
def objective_normal(c,p_min):
    
    print("c")
    print(c)
    print(p_min)
    
    E_data = {'busthigh': 0.8,
              'boomhigh': 0.9,
              'bustlow': 0.8,
              'boomlow': 0.7}
    
    c = {'low': [c[0],c[1]],
         'high': [c[2],c[3]] }
    
    mu = [0.8,0.4]
    sigma = [0.1,0.1]
    
    obj = 0
    E_sim = dict()
    for spec in ['low','high']:
        for b in ['bust','boom']:
            E_sim[b+spec] = copy.copy( E_normal(c[spec],mu,sigma,p_min[b],spec) )
            obj += (E_data[b + spec] - E_sim[b + spec])**2
            
    print(E_sim)
    print(obj)
    
    return obj

def objective(c,rho,p_min):
    E_data = {'busthigh': 0.82,
              'boomhigh': 0.9,
              'bustlow': 0.78,
              'boomlow': 0.7}
    
    c = {'low': [c[0],c[1]],
         'high': [c[2],c[3]] }
    
    v = [0,0]
    
    obj = 0
    E_sim = dict()
    for spec in ['low','high']:
        for b in ['bust','boom']:
            E_sim[b+spec] = E(c[spec],rho,p_min[b],spec)
            obj += (E_data[b + spec] - E_sim[b + spec])**2
            
    print(E_sim)
    print(obj)
    
    return obj

def minimize():
    
    p_min = {'bust': 0.05,
             'boom': 0.1}
    
    #rho = [0.5,0.5]
    
    #c_init = [0,
     #         rho[1] + 0.0001,
      #        0,
       #       rho[1] - 0.0001]
    
    c_init = [0,
              0,
              0,
              0]
    
    c_bounds = [(0,None),(0,None),(0,None),(0,None)]
    
    result = scipy.optimize.minimize(objective_normal,
                                     c_init,
                                     bounds = c_bounds, 
                                     args = (p_min))
    
    print(result)
    
minimize()