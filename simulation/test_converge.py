import converge
import numpy as np
import pytest

@pytest.mark.skip()
@pytest.fixture()
def test_meet():
    meet_params = {'a': np.array([1,1,1]),
                   'd': np.array([10.0,10,10,10]) }
    
    return(converge.Meet(meet_params))

@pytest.mark.skip()
def test_meet_2(meet): 
    state = [1,np.array([[0.2,0.2,0.2],
                         [0.4,0.4,0.4],
                         [0.4,0.4,0.4]])]
    
    assert( (meet.q_x().round(2) == [0.45,0.45,0.45]).all() ) 

@pytest.fixture()
@pytest.mark.skip()
def test_demand_uniform(meet):
    demand_params = {'mu': 0, 
                     'sigma': 1, 
                     'gamma': np.array([1,1,1])*0,
                     'c': 0}
    
    def surplus(x):
        surplus = np.array([0.5 - 1*x ,0.5,-0.5 + 1*x])
        surplus[surplus < 0] = 0
        return(surplus)
    
    demand = converge.Demand(demand_params, meet, surplus)
    
    demand.f = lambda x : (x <= 1)
    
    return(demand)

@pytest.mark.skip()
def test_compute_target_pdf(demand_uniform):
    demand_uniform.compute_target_pdf()
    
    assert( (demand_uniform.meet.shares.round(2) == [0.33,0.33,0.33] ).all() )

@pytest.mark.skip()   
def test_compute_match(demand_uniform):
    (accept_y,expect_y,match_y) = demand_uniform.compute_match()
    
    assert (accept_y.round(2) == [0.5,1.0,0.5] ).all() 
    assert (expect_y.round(2) == [0.25,0.5,0.75] ).all() 
    assert (match_y.round(2) == [0.38,0.75,0.38] ).all() 

@pytest.fixture()
@pytest.mark.skip()
def test_model_uniform(meet, demand_uniform):

    meet_params = {'a': np.array([1,1,1]),
                   'd': np.array([10.0,10,10,10]) }
     
    demand_params = {'mu': 0, 
                 'sigma': 1, 
                 'gamma': np.array([1,1,1])*0,
                 'c': 0} 
    e = 0.3
    
    def surplus(x,state):
        surplus = np.array([0.5 - 1*x-(1/10)*state[0] ,0.5-(1/10)*state[0],-0.5 + 1*x-(1/10)*state[0]])
        surplus[surplus < 0] = 0
        return(surplus)

    model = converge.Model(demand_params, meet_params, e, surplus)
    
    model.demand.f = lambda x : (x <= 1)
    
    return(model)

@pytest.mark.skip()
def test_one_period(model_uniform):
    
    model_uniform.one_period()
    
    assert (model_uniform.expect_y.round(2) == [0.2,0.5,0.8] ).all() 
    assert (model_uniform.accept_y.round(2) == [0.4,1.0,0.4] ).all() 
    assert (model_uniform.match_y.round(2) == [0.22,0.55,0.22] ).all() 
    
def test_cutoff():
    match_params = {'delta': 0.5,
                     'low0': 0,
                     'low1': 0.172,
                     'mid0': 0,
                     'mid1': 0,                                 
                     'high0': 0.04,
                     'high1': 0}
    match_params['eta'] = 0.5

    meet_params = {'d': [[10.26,10.288,-0.19], 20, 20, 20 ],
                    'a': np.array([0.1155,0.0814,0.0215]) }

    demand_params = {'gamma': np.array([0,0,0]),
                      'c': 0.06,
                      'e': 0.5,
                      'mu': [0.8], 
                      'sigma': [1],
                      'v': [0.09**2,0.05], 
                      'p_tau': {2: 1/3, 3: 1/3, 4: 1/3} }

    demand = converge.Demand(match_params, 
                             meet_params, 
                             demand_params, 
                             1, 
                             n_total = np.array([20,20,20]))
    
    cutoff = demand.cutoff(tau=3,
                           p_min = 0,
                           spec = 'low')
    
    print(cutoff)

test_cutoff()