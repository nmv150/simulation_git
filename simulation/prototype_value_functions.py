
import numpy as np
import matplotlib.pyplot as plt
import value_functions
import scipy.optimize
import pandas as pd
import collections
import lmfit

def proto_price():
    def price(beta,delta,eta,tau,q):
        s = 0.2 + q # = 0.5 to 1.2
        a = beta*(1-beta**tau*eta) - beta**tau*(1-eta)
        b = (1-beta**tau*eta)*(1-beta)
        
        num = b + a*q
        denom = b + delta*a*q
        
        return delta*s*(num/denom)
    
    beta = 0.99
    delta = 0.5
    eta = 0.5
    tau = 3
    p = np.linspace(0.3,1,20)
    
    plt.plot(p,price(beta,delta,eta,tau,p))
    plt.show()
    
params = {'delta': 0.5, 'c': [0,-0.05], 'eta': 0.5}
spec_use = ['low-spec',
            'mid-spec',
            'high-spec']

model = value_functions.Model(eta = 0.5)
#model.plot_fit_match()
#model.fit_v(params,spec_use)
#model.estimate_v(x_bounds = None, verbose = True, use_ss = False, plot = True)
#model.plot_contour_match()
 
model.estimate_v_lmfit(plot = True)