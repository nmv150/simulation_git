import numpy
import quadpy
import scipy.stats

scheme = quadpy.line_segment.gauss_patterson(5)
#scheme.show()
print(scheme.points)

def f(x):
    print(x)
    return numpy.exp(x)*scipy.stats.norm.pdf(x,1,1)

val = scheme.integrate(f, [0.0, 1.0])

print(val)