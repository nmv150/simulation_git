import converge2 as converge
import numpy as np
from scipy.stats import norm, multivariate_normal

def simulation(demand_params,meet_params,surplus):
    
    model = converge.Estimation(demand_params,meet_params,surplus)
    
    model.demand.f = lambda x: (x <= 1)*1
    
    model.burn_in()
    
    for k in range(0,20):
        model.demand.state[0] = k/5
        model.demand.meet._d[0] = 10 + k/5

        model.one_period()
        print('Shares: ' + str(model.demand.meet.shares.round(2)),'q_y: ' + str(model.demand.meet.q_y().round(2)),'match_y: ' + str(model.match_y.round(2)),'E_y: ' + str(model.expect_y.round(2)) )
        #print(k, model.expect_y.round(2), 1-model.state[1][0,:].round(2)) 

def surplus(x,state):
    surplus = np.array([1,1 + state[0]/5,1])
    surplus[surplus < 0] = 0
    surplus = surplus*np.array([(x <= 1),(x <= 1),(x <= 1)])
    surplus = surplus*np.array([(x >= 0),(x >= 0),(x >= 0)])
    return(surplus)
    
#simulation(demand_params,meet_params,surplus)
    
model_test = converge.Estimation()
model_test.estimate(use_saved_model = True)
#model_test.plot_min_price(use_saved_model=True)
#model_test.plot_v(use_saved_model=True)
    
#counterfactuals = converge.Counterfactuals()
#counterfactuals.do_no_sorting() 

#cutoff = model_test.demand.cutoff(3,p_min = 0.05, spec = 'low')
#print(cutoff)