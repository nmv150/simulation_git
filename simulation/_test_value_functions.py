import value_functions
import numpy as np
import pytest
import pandas as pd
import itertools

def test_fit():
    match = {'tau': lambda s: (1-np.exp(-s[0]/10))*np.array([(1/3),
                                                             (1/3),
                                                             (1/3),]),
            'mean_value': lambda s, tau: 0,
            'mean_mri': lambda s, tau: 1
            }
    eta = 0.4
    delta = 0.5
    c = [0,0]
        
    V = value_functions.V(match,eta,c,delta,T=5)
    
    state_eval = np.array([7,10,10,10])
    assert V.v_interp(state_eval).round(1) == 0.00
    
def test_fit_1():
    # Test when value = 0 but mri != 0, constant p_match, no extend
    
    match = {'tau': lambda s: np.array([0,0.5,0,]),
            'mean_value': lambda s, tau: 0,
            'mean_mri': lambda s, tau: 1
            }
    eta = 0
    delta = 0.5
    c = [-1,-1]
        
    V = value_functions.V(match,eta,c,delta,T=5)
    
    state_eval = np.array([1,5,5,5])
    
    def v_solution(beta,delta): 
        def denom(beta,delta):
            return 1-(0.5*delta*(beta**3-beta)+beta)
        return (0.5*delta*(-2)*(1+beta+beta**2))/denom(beta,delta)
    
    assert V.v_interp(state_eval).round(1) == round(v_solution(0.99,delta),1)
    

def test_fit_2(): 
    # Test with moving gas price and nonzero mean value, but keeping p_match constant and state evolution constant
    match = {'tau': lambda s: np.array([0,0.5,0,]),
            'mean_value': lambda s, tau: 1,
            'mean_mri': lambda s, tau: 0
            }
    eta = 0
    delta = 0.5
    c = [0,-1]
    
    const = np.zeros((4,1))
    R = np.eye(4) 
        
    V = value_functions.V(match,eta,c,delta,const,R,T=5)
    
    g_eval = 1
    state_eval = np.array([g_eval,10,10,10])
    
    def v_solution(beta,delta,g):
        b = 0.5*delta/(1-(0.5*delta*(beta**3-beta)+beta))
        
        return b*beta**3*g
    
    assert V.v_interp(state_eval).round(1) == round(v_solution(0.99,delta,g_eval),1)

def test_fit_3(): 
    # Test with differing gas price, eta != 0 , but keeping p_match constant and state evolution constant
    match = {'tau': lambda s: np.array([0,0.5,0,]),
            'mean_value': lambda s, tau: 1,
            'mean_mri': lambda s, tau: 0
            }
    eta = 0.5
    delta = 0.5
    c = [0,-1]
    
    const = np.zeros((4,1))
    R = np.eye(4) 
        
    V = value_functions.V(match,eta,c,delta,const,R,T=100)
    
    g_eval = 15
    s = np.array([g_eval,5,5,5])
    
    def v_solution_3(s,p,eta,beta,delta,g):
        
        num = ( V.cost_total(match['mean_mri'](s,3),3)+beta**3*g*match['mean_value'](s,3))
        denom = ( (1-beta)/(p*delta) )*(1-beta**3*eta) + beta - beta**3*(1-eta)
        
        return num/denom

    assert V.v_interp(s).round(1) == round(v_solution_3(s,match['tau'](s)[1],eta,0.99,delta,g_eval),1)

def test_fit_4(): 
    # Test with moving gas price and nonzero mean value, but keeping p_match constant, state evolving
    match = {'tau': lambda s: np.array([0,0.5,0,]),
            'mean_value': lambda s, tau: 1,
            'mean_mri': lambda s, tau: 0
            }
    eta = 0
    delta = 0.5
    c = [0,0]
    p = 0.5
        
    Vk = value_functions.V(match,eta,c,delta,T=100)
    
    g_eval = 1
    state_eval = np.array([g_eval,5,5,5])
    
    def v_solution_1(beta,delta,p,g):
        l_0 = 0.64
        l_1 = 0.89
        
        l_star = l_0+l_1*l_0+l_1**2*l_0

        b = p*delta*beta**3*l_1**3/(1-p*delta*beta**3*l_1**3-(1-p*delta)*beta*l_1)
        a = (p*delta*beta**3*(1+b)*l_star + (1-p*delta)*beta*b*l_0)/(1-p*delta*beta**3-(1-p*delta)*beta)

        return a + b*g
    
    assert Vk.v_interp(state_eval).round(1) == round(v_solution_1(0.99,delta,p,g_eval),1)

def test_surplus():
    match = {'tau': lambda s: (1-np.exp(-s[0]/10))*np.array([(1/3),
                                                             (1/3),
                                                             (1/3),]),
            'mean_value': lambda s, tau: 0,
            'mean_mri': lambda s, tau: 1
            }
    eta = 0.4
    c = [0,0]
    delta = 0.5
        
    V = value_functions.V(match,eta,c,delta)
    
    V.c = c
    s = (0,5,5,5)
    V.v_interp.values = np.ones((5,5,5,5))*0
    x = {'mri': 1, 'value':0, 'tau':3}
    
    assert V.surplus(s,x) == 0

def test_surplus_1():
    match = {'tau': lambda s: (1-np.exp(-s[0]/10))*np.array([(1/3),
                                                             (1/3),
                                                             (1/3),]),
            'mean_value': lambda s, tau: 1,
            'mean_mri': lambda s, tau: 1
            }
    eta = 0.5
    c = [-1,-1]
    delta = 0.5
        
    V = value_functions.V(match,eta,c,delta,fit=False)
    
    V.c = c
    s = (0,5,5,5)
    V.v_interp.values = np.ones((5,5,5,5))*1
    x = {'mri': 1, 'value':2, 'tau':3}
    
    V_evol = np.ones((1,V.T))[0]*1
    points_evol = np.ones((V.T,1))*1
    
    def v_solution(beta): 
        return ((1+beta+beta**2)*(-2)+beta**3*2+(1-0.5)*beta**3*1-beta*1)/(1-0.5*beta**3)
    
    assert round(V.surplus(s,x,V_evol,points_evol),2) == round(v_solution(0.99),2)

def test_surplus_2():
    match = {'tau': lambda s: (1-np.exp(-s[0]/10))*np.array([(1/3),
                                                             (1/3),
                                                             (1/3),]),
            'mean_value': lambda s, tau: 0,
            'mean_mri': lambda s, tau: 1
            }
    eta = 0
    c = [-1,-1]
    delta = 0.5
        
    V = value_functions.V(match,eta,c,delta,fit=False)
    
    V.c = c
    s = (0,5,5,5)
    V.v_interp.values = np.ones((5,5,5,5))*1
    x = {'mri': 1, 'value':0, 'tau':3}
    
    V_evol = np.ones((1,V.T))[0]*1
    points_evol = np.ones((V.T,1))*1
    solution = (1+0.99+0.99**2)*(-2) + 0.99**3 - 0.99*1
    
    assert V.surplus(s,x,V_evol,points_evol) == solution

def test_price_0():
    # Use all the stuff from the constant V test.
    match = {'tau': lambda s: np.array([0,0.5,0,]),
            'mean_value': lambda s, tau: 1,
            'mean_mri': lambda s, tau: 0
            }
    delta = 0.5
    c = [0,-1]
    p = 0.5
    beta = 0.99
    
    const = np.zeros((4,1))
    R = np.eye(4) 
    
    def v_solution_3(s,p,eta,beta,delta,g):
        
        num = ( beta**3*g*match['mean_value'](s,3))
        denom = ( (1-beta)/(p*delta) )*(1-beta**3*eta) + beta - beta**3*(1-eta)
        
        return num/denom
    
    g_eval = 5
    s = np.array([g_eval,5,5,5])
    x = {'mri':0,'value':1,'tau':3}
    
    # Test eta = 0  
    eta = 0
    V_0 = value_functions.V(match,eta,c,delta,const,R,T=100)
    
    def price_eta_0(s,p,eta,beta,delta,g):
        
        v_0 = v_solution_3(s,p,eta,beta,delta,g) 
        
        return ( delta*( beta**3*g*match['mean_value'](s,3)) 
               + (1-delta)*(beta*v_0-beta**3*v_0) )
    
    assert round(price_eta_0(s,p,eta,beta,delta,g_eval),3) == round(V_0.price(s,x),3)

def test_price_1():
    # Use all the stuff from the constant V test.
    match = {'tau': lambda s: np.array([0,0.5,0,]),
            'mean_value': lambda s, tau: 1,
            'mean_mri': lambda s, tau: 0
            }
    delta = 0.5
    c = [0,-1]
    p = 0.5
    beta = 0.99
    
    const = np.zeros((4,1))
    R = np.eye(4) 
    
    def v_solution_3(s,p,eta,beta,delta,g):
        
        num = ( beta**3*g*match['mean_value'](s,3))
        denom = ( (1-beta)/(p*delta) )*(1-beta**3*eta) + beta - beta**3*(1-eta)
        
        return num/denom
    
    g_eval = 5
    s = np.array([g_eval,5,5,5])
    x = {'mri':0,'value':1,'tau':3}
    # Test eta = 1  
    eta = 0.5
    V_1 = value_functions.V(match,eta,c,delta,const,R,T=300)
    v_evol = np.ones((1,100))[0]*5 
    
    def price_eta(s,p,eta,beta,delta,g):
        
        v = v_solution_3(s,p,eta,beta,delta,g) 
        v = 5
        
        #print(( beta*(1-beta**3*eta) - beta**3*(1-eta) ))
        
        return ( delta*( beta**3*g*match['mean_value'](s,3) ) 
                + (1-delta)*( beta*(1-beta**3*eta) - beta**3*(1-eta) )*v )
    
    print( round(price_eta(s,p,eta,beta,delta,g_eval),3) )
    print( round(V_1.price(s,x,v_evol ),3)) 
    
    assert round(price_eta(s,p,eta,beta,delta,g_eval),3) == round(V_1.price(s,x,v_evol),3)
    