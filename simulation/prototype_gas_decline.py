import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize

def monthly_compute(Q,t):
    beta = 0.99
    a0 = 0.337
    a1 = -0.0269
    beta = 0.144
    gamma0 = 3.97
    gamma1 = -0.319
    
    q = Q*(a0+a1*Q)*(1+beta*t)**(-gamma0+gamma1*Q)
    
    return q

def monthly_compute_exponential(Q,t):
    yearly_decline = 0.5
    D = (1+ yearly_decline)**(1/12)-1
    
    q = Q*np.exp(-1*D*t)
    
    return q

def monthly_compute_exponential_prop(Q,t):
    #results = [-0.22544629, 1.05505702]
    results =  [-0.25527692, 1.137]
    
    prop = results[0]*np.log(t) + results[1]
    q = Q*prop
    
    return q

def compute_lifetime(g,Q):
    total = 0
    beta = 0.99
    for t in range(1,40):
        total += g*(beta**t)*monthly_compute_exponential_prop(Q,t)
        g = 0.64 + 0.89*g
        
    return(total)

def plot():
    for Q in [1]: #np.linspace(0.02,0.1,5):
        g_sim = np.linspace(0,10,30)
        value_sim = []
        for g in g_sim:
            value_sim.append( compute_lifetime(g,Q) )
        value_sim = np.array(value_sim)
        plt.xlabel('Gas Price')
        plt.ylabel('Value')
        plt.plot(g_sim,value_sim)
        print('slope:')
        print( (value_sim[-1] - value_sim[0])/value_sim[0] )
        
        print('initial:')
        print(value_sim[0])
        
        print('final:')
        print(value_sim[-1])
        
        
        #print('linear prop change:')
        #print( (g_sim[-1] - g_sim[0])/g_sim[0] )
    plt.show()

def cutoff(rho_0,rho_1,p_min,c_0,c_1,g):
    num = p_min + c_0
    denom = g*rho_1 + rho_0 - c_1
    return num/denom

def expectation(rho_0,rho_1,p_min,c_0,c_1,g):
    return (1.6 + cutoff(rho_0,rho_1,p_min,c_0,c_1,g))/2

def compute_sorting_uniform(x):
    
    print(x)
    
    c_0_high,c_1_high = x
    rho_0 = 5.243
    rho_1 = 0.1111

    p_min = {'boom': 0.1,
             'bust': 0.05}
    g = {'boom': 8,
         'bust': 2}
    c_0 = {'low': 0.1,
           'high': c_0_high}
    
    c_1 = {'low': 0.1,
           'high': c_1_high}
    E_data = {'boomlow': 0.7,
              'bustlow': 0.75,
              'boomhigh':0.9,
              'busthigh':0.85}
    E = dict()
    output = 0
    for b in ['bust','boom']:
        for spec in ['high']: 
            E[b + spec] = expectation(rho_0,rho_1,p_min[b],c_0[spec],c_1[spec],g[b])
            output += (E_data[b+spec] -  E[b+spec])**2

    print(E)
    print(output)
    return output

#x_init = [-0.26691, 4.26899991] # SOLUTION WITH MEDIAN rho_0 = 1.661 rho_1 = 0.222
#x_init = [1,0]
#x_bounds = [(-0.3,-0.1),(0,5)]

#result = scipy.optimize.minimize(compute_sorting_uniform, x_init,bounds=x_bounds)

#print(result)
plot()   