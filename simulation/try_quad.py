import numpy as np
from scipy.stats import norm, multivariate_normal
import scipy.optimize
from numpy.linalg import inv
import matplotlib.pyplot as plt
import emcee

##############
# MCMC STUFF #
##############

def lnprob_trunc_norm(x, mean, bounds, C):
    return -0.5*(x-mean).dot(inv(C)).dot(x-mean)
    
    #if np.any(x < bounds[:,0]) or np.any(x > bounds[:,1]):
     #   return -np.inf
    #else:
     #   return -0.5*(x-mean).dot(inv(C)).dot(x-mean)
    
def do_emcee():
    mean = np.array([0.5,0.5])
    C = np.array([[1,0],
                  [0,1]])
    bounds = np.array([[0,1],
                       [0,1]])
    
    Nwalkers = 50
    Ndim = 2
    
    S = emcee.EnsembleSampler(Nwalkers, Ndim, lnprob_trunc_norm, args = (mean, bounds, C))
    pos = np.random.multivariate_normal(mean, C, size=Nwalkers)
    Nsteps = 10000

    state = S.run_mcmc(pos, 10000)
    
    print(state)
    
    S.reset()
    S.run_mcmc(state, Nsteps)

    samples = S.get_chain(flat=True)

    #samples = samples[samples[:,0] < 1]
    #samples = samples[samples[:,0] > 0]
    
    plt.hist(samples[:, 0], color="k")
    
    print(samples.mean(axis=0))

def cutoff_contour(x,c,p_min):
    
    contour = p_min + c[0] + c[1]*x
    
    return contour

def multi_normal(x_value,x_mri,mu,sigma):
    normal = multivariate_normal.pdf([x_mri,x_value],mu,sigma)
    #print('mri: ' + str(x_mri))
    #print('value: ' + str(x_value))

    return(normal)

def E_bivariate_normal(c,mu,sigma,spec,p_min):
    f = lambda x_value, x_mri: multi_normal(x_value,x_mri,mu,sigma)
    xf = lambda x_value, x_mri: x_mri*multi_normal(x_value,x_mri,mu,sigma)
    gfun = lambda x_mri: cutoff_contour(x_mri,c,p_min)
    hfun = lambda x_mri: cutoff_contour(x_mri,c,p_min) + 10
    
    int_f = scipy.integrate.dblquad(f, a = 0, b = 1.6, gfun = gfun, hfun = hfun)[0]
    int_xf = scipy.integrate.dblquad(xf, a = 0, b = 1.6, gfun = gfun, hfun = hfun)[0]

    return int_xf/int_f

def try_quad():
    c = [0.00838437,1.21350966]
    p_min = 0.05
    spec = 'low'
    
    mu = [0.8,1.2]
    sigma = [[1,0.99],
             [0.99,1]]
    
    result = E_bivariate_normal(c,mu,sigma,spec,p_min)
    print(result)
    
try_quad()
    