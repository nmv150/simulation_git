import numpy as np
import scipy.optimize
from scipy.stats import truncnorm
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
import copy

def cutoff_contour(x,c,p_min):
    return p_min + c[0] + c[1]*x

def E_bivariate_normal(c,mu,sigma,p_min,spec):
    f = lambda x_mri,x_value: multivariate_normal.pdf([x_mri,x_value],mu,sigma)
    xf = lambda x_mri, x_value: x_mri*f(x_mri,x_value)
    gfun = lambda x_mri: cutoff_contour(x_mri,c,p_min)
    hfun = lambda x_mri: cutoff_contour(x_mri,c,p_min) + 1
    
    int_f = scipy.integrate.dblquad(f, a = 0, b = 1.6, gfun = gfun, hfun = hfun)[0]
    int_xf = scipy.integrate.dblquad(xf, a = 0, b = 1.6, gfun = gfun, hfun = hfun)[0]

    return int_xf/int_f

def E_bivariate_normal_montecarlo(c,mu,sigma,p_min,spec):
    np.random.seed(1234)
    f_sample = np.random.multivariate_normal(mu,sigma,size = 100000)
    f_sample = f_sample[np.where( (f_sample[:,0] >= 0) & (f_sample[:,0] <= 1.6))]   
    f_sample = f_sample[np.where(f_sample[:,1] >= 0)]  
    
    f_sample_profit = f_sample[:,1] - p_min - c[0] - c[1]*f_sample[:,0]
    
    f_sample = f_sample[np.where(f_sample_profit >= 0)]   
    f_sample_profit = f_sample_profit[np.where(f_sample_profit >= 0)]  

    print('cutoff',f_sample[:,0].min(),f_sample[:,0].max())

    #plt.scatter(f_sample[:,0],f_sample_profit)
    #plt.title(spec)

    return f_sample.mean(axis=0)[0]

def cutoff(c,rho,p_min):
    
    num = p_min - (rho[0]-c[0])
    denom = (rho[1]-c[1])
    cutoff = max(0,num/denom)
    cutoff = min(1.6,cutoff)
    return cutoff

def E(c,rho,p_min,spec):
    if spec == 'low':
        #if cutoff(c,rho,p_min) <= 1.6:
        return cutoff(c,rho,p_min)/2 
        
        #if cutoff(c,rho,p_min) > 1.6:
         #   return 0.8
    
    if spec == 'high':
        return (1.6 + cutoff(c,rho,p_min))/2
    
def E_normal(c,mu,sigma,p_min,spec):
    rho = []
    rho.append( mu[1] - mu[0]*(sigma[1]/sigma[0]) )
    rho.append( sigma[1]/sigma[0] )
    cutoff_normal = cutoff(c,rho,p_min)
    
    print("CUTOFF")
    print(spec,cutoff_normal)
    
    if spec == 'low':
        return truncnorm.mean(0, cutoff_normal, loc=mu[0], scale=sigma[0])
    
    if spec == 'high':
        return truncnorm.mean(cutoff_normal, 1.6, loc=mu[0], scale=sigma[0])
    
def objective_normal(c,p_min):
    
    print("c")
    print(c)
    print(p_min)
    
    E_data = {'busthigh': 0.8,
              'boomhigh': 0.9,
              'bustlow': 0.8,
              'boomlow': 0.7}
    
    c = {'low': [c[0],c[1]],
         'high': [c[2],c[3]] }
    
    mu = [0.8,0.4]
    sigma = [0.1,0.1]
    
    obj = 0
    E_sim = dict()
    for spec in ['low','high']:
        for b in ['bust','boom']:
            E_sim[b+spec] = copy.copy( E_normal(c[spec],mu,sigma,p_min[b],spec) )
            obj += (E_data[b + spec] - E_sim[b + spec])**2
            
    print(E_sim)
    print(obj)
    
    return obj

def objective_bivariate_normal(c,p_min):
    E_data = {'busthigh': 0.82,
              'boomhigh': 0.9,
              'bustlow': 0.78,
              'boomlow': 0.7}
    
    c = {'low': [c[0],c[1]],
         'high': [c[2],c[3]] }
    
    mu = [0.8,1]
    
    corr = 1
    sigma_mri = 0.4
    sigma_value = 0.5
    
    cov = corr*sigma_mri*sigma_value
    sigma = np.array([[sigma_mri**2,cov],
                      [cov,sigma_value**2]])
    
    obj = 0
    E_sim = dict()
    for spec in ['low','high']:
        for b in ['bust','boom']:
            E_sim[b+spec] = E_bivariate_normal_montecarlo(c[spec],mu,sigma,p_min[b],spec) 
            obj += (E_data[b + spec] - E_sim[b + spec])**2
            plt.show()
            
    print(E_sim)
    print(obj)
    
    return obj

def objective(c,rho,p_min):
    E_data = {'busthigh': 0.82,
              'boomhigh': 0.9,
              'bustlow': 0.78,
              'boomlow': 0.7}
    
    c = {'low': [c[0],c[1]],
         'high': [c[2],c[3]] }
    
    obj = 0
    E_sim = dict()
    for spec in ['low','high']:
        for b in ['bust','boom']:
            E_sim[b+spec] = E(c[spec],rho,p_min[b],spec)
            obj += (E_data[b + spec] - E_sim[b + spec])**2
            
    print(E_sim)
    print(obj)
    
    return obj

def minimize():
    
    p_min = {'bust': 0.05,
             'boom': 0.1}
    
    #rho = [0.5,0.5]
    
    #c_init = [0,
     #         rho[1] + 0.0001,
      #        0,
       #       rho[1] - 0.0001]
    
    c_init = [0,
              0,
              0,
              0]
    
    c_bounds = [(0,None),(0,None),(0,None),(0,None)]
    
    result = scipy.optimize.minimize(objective_bivariate_normal,
                                     c_init,
                                     bounds = c_bounds, 
                                     args = (p_min))
    
    print(result)
    
minimize()