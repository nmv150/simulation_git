import numpy as np
import scipy.optimize
from scipy.stats import truncnorm
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
import copy

def find_cutoff(c,rho,p_min):    
    
    def construct_objective():    
        def objective(x):
            objective_result = (p_min 
                                + c[0] 
                                + c[1]*x
                                - np.sqrt(rho[0] + rho[1]*x) )**2
            return objective_result
        return objective
    
    objective_fn = construct_objective()

    x0 = 0.5
    #x_bounds = [(0,1.6)]
    
    cutoff = scipy.optimize.root(objective_fn,x0).x[0]
    
    #print("RESULT CUTOFF",cutoff)
        
    return cutoff

def trunc_normal_mean(lower,upper,mu,sigma):
    
    alpha = (lower - mu)/sigma
    beta = (upper - mu)/sigma
    
    numerator = scipy.stats.norm.pdf(alpha,0,1) - scipy.stats.norm.pdf(beta,0,1)
    denominator = scipy.stats.norm.cdf(beta,0,1) - scipy.stats.norm.cdf(alpha,0,1)
    
    print("NUM",numerator)
    print("DENOM",denominator)
    
    return mu + sigma*(numerator/denominator)

def E(c,rho,p_min,spec):

    cutoff_normal = find_cutoff(c,rho,p_min)
    
    print("")
    print("CUTOFF",spec,cutoff_normal)
    
    if spec == 'low':
        output = trunc_normal_mean(0, cutoff_normal, 0.8, 1)
        print("EXP",spec,output)
        return output
    
    if spec == 'high':
        output = trunc_normal_mean(cutoff_normal, 1.6, 0.8, 1)
        print("EXP",spec,output)
        return(output)

def objective(x,p_min):
    E_data = {'busthigh': 0.8,
              'boomhigh': 0.9,
              'bustlow': 0.8,
              'boomlow': 0.7}
    
    c = {'low': [x[0],x[1]],
         'high': [x[2],0] }
    
    rho = [(0.05 + x[2])**2,x[3]]
    
    print("C",c)
    print("RHO",rho)
    
    obj = 0
    E_sim = dict()
    for spec in ['low','high']:
        for b in ['bust','boom']:
            E_sim[b+spec] = E(c[spec],rho,p_min[b+spec],spec)
            obj += (E_data[b + spec] - E_sim[b + spec])**2
    
    print("")
    print("MOMENTS")
    print(E_sim)
    print(obj)
    
    return obj

def minimize():
    
    p_min = {'busthigh': 0.05,
             'boomhigh': 0.1,
             'bustlow': 0.02,
             'boomlow': 0.04 }
    
    #rho = [0.5,0.5]
    
    #c_init = [0,
     #         rho[1] + 0.0001,
      #        0,
       #       rho[1] - 0.0001]
    
    x_init = [0,
              0.17,
              0.01,
              0.01]
    
    x_bounds = [(0,0.000001),(0,2),(0,0.1),(0,0.1)]
    
    objective(x_init,p_min)
    
    result = scipy.optimize.minimize(objective,
                                     x_init,
                                     bounds = x_bounds, 
                                     args = (p_min))
    
    print(result)
    
minimize()