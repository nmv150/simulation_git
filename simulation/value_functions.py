import numpy as np
import pandas as pd
import scipy.interpolate
import itertools
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LogisticRegression
import lmfit
import csv
import dill

class State():
    def __init__(self,const=None,R=None):
        
        if (const is None):
            const = np.array([[0.64],[4.06],[5.52],[8.90]])
        self.const = const
        
        if (R is None):
            R = np.array([[0.89, 0, 0, 0],
                          [-0.24, 0.83, -0.027, 0.09],
                          [-0.469, 0.17, 0.52, 0.12],
                          [-0.467, -0.018, 0.021, 0.36]])                                                                                                                                              
        self.R = R
    
    def next_state(self,s):
        s_next =  self.const + self.R @ s.reshape((-1,1))
        return s_next.T[0]

    def evol(self,s0,t,as_dataframe = False):
        if type(s0) == tuple:
            s0 = np.array(s0)      
            
        states = [s0]
        s = s0
        for t in range(0,t-1):
            s_next = self.next_state(s)
            states.append(s_next)
            s = s_next
            
        if (as_dataframe == False):
            return np.array(states)
        
        elif (as_dataframe == True):
            state_data = pd.DataFrame(states)
            state_data.columns = ['g','nl','nm','nh']
            return(state_data)
    
class V():                                              
    def __init__(self,match,eta,c,delta,const=None,R=None,T=100,fit=True,spec='',data = None,use_empirical_states=True):
        self.T = T
        self.beta = 0.99
        self.g_grid = np.linspace(2,15,5)
        self.n_grid = np.linspace(10,25,5)
        self.state = State(const,R)
        self.match = match
        self.eta = eta
        self.c = c
        self.delta = delta
        self.spec = spec
        self.data = data
        
        # Initialize state evolution 
        
        pts = (self.g_grid,
               self.n_grid,
               self.n_grid,
               self.n_grid)
        
        vals = np.ones((len(self.g_grid),
                        len(self.n_grid),
                        len(self.n_grid),
                        len(self.n_grid)))
        
        self.v_interp = scipy.interpolate.RegularGridInterpolator(pts,vals,method='linear',bounds_error=False,fill_value=None)  
        
        if use_empirical_states == True:
            self.points_evol_empirical_states = dict()
            self.s_all_empirical_states_list = []
            points_evol_empirical_states_list = []
            for s in data[['g','n_l','n_m','n_h']].itertuples():
                s = (s.g,s.n_l,s.n_m,s.n_h)
                self.points_evol_empirical_states[s] = self.state.evol(s,self.T)
                points_evol_empirical_states_list.append(self.points_evol_empirical_states[s])
                self.s_all_empirical_states_list.append(s)
                
            self.points_evol_empirical_states_all = np.concatenate(points_evol_empirical_states_list)
        
        if fit == True:   
            self.points_evol = dict()
            self.s_all_list = []
            points_evol_list = []
            self.cost_dict = dict()
            self.p_dict = dict()
            self.v_dict = dict()
            self.dayrate_evol_dict = dict()
            
            for s in itertools.product(self.g_grid,self.n_grid,self.n_grid,self.n_grid):
                #self.cost_dict[s] = {tau: self.cost_total(self.match['mean_mri'](s,tau),tau) for tau in [2,3,4]}
                self.p_dict[s] = self.match['tau'](s)
                #self.v_dict[s] = {tau: self.match['mean_value'](s,tau) for tau in [2,3,4]}   
                
                self.points_evol[s] = self.state.evol(s,self.T)
                points_evol_list.append(self.points_evol[s])
                self.s_all_list.append(s)

                self.dayrate_evol_dict[s] = {tau: np.apply_along_axis(lambda point: self.match['mean_dayrate'](point,tau), 
                                                                      axis = 1, 
                                                                      arr = self.points_evol[s]) for tau in [2,3,4]} 
            
        if fit == True:
            self.points_evol_all = np.concatenate(points_evol_list)
            
        self._init_A()
        
        # Fit interpolation
        
        if fit == True:              
            self.fit_v_use_price()
            
        if use_empirical_states == True:
            self.v_evol_empirical_states = self._apply_v_empirical_states()    
            
        # Set up some stuff for getting the residuals
        
        if self.data is not None:
            
            self.data_points_evol = dict()
            self.data_s_all_list = []
            data_points_evol_list = []
            for i in self.data.itertuples():
                s = (i.g,i.n_l,i.n_m,i.n_h)
                self.data_points_evol[s] = self.state.evol(s,self.T+4)
                data_points_evol_list.append(self.data_points_evol[s])
                self.data_s_all_list.append(s)
            
            self.data_points_evol_all = np.concatenate(data_points_evol_list)
            
        
            
    def __str__(self):
        return 'Value of Search | Params = (spec: {}, eta: {}, c: {}, delta: {})'.format(self.spec,self.eta,self.c,self.delta)
    
    def _value(self,x,points_evol):
        
        #GAS PRICE DECLINE PARAMS:
        rho_0 = 1
        rho_1 = 0

        x['value'] = 0 #self.c[2]*x['mri'] + self.c[3]
        
        return x['value']*(rho_0 + rho_1*np.dot(self.A[x['tau']],points_evol[:,0])) 
    
    def cost_total(self,mri,tau): 
        cost = self.c[0] + self.c[1]*mri #+ self.c[3]*mri**2
        
        ###########################################
        #NOTE : NEED TO CHANGE TO 1 + beta etc... #
        ###########################################

        #beta = self.beta
        
        #if tau == 2:
         #   return (1+beta)*cost
        
        #if tau == 3:
         #   return (1+beta+beta**2)*cost
        
        #if tau == 4:
         #   return (1+beta+beta**2+beta**3)*cost
        
        return cost
    
    def _guess_v(self,s,detail=False):
        # Intial guess: steady state of tau = 3 contract.
        
        #print(self.match)
        
        p = self.p_dict[s][1]
        a = self.beta*(1-self.beta**3*self.eta) - self.beta**3*(1-self.eta)
        b = (1-self.beta**3*self.eta)*(1-self.beta)
        
        denom = b + a*p*self.delta
        
        num = self.delta*p*( self.cost_dict[s][3]
                           + self.beta**3*s[0]*self.v_dict[s][3] ) 
        
        if (detail == True):
            return(num,denom,num/denom)
        
        else:
            return num / denom
    
    def fit_guess_v(self):
        v_new = np.empty_like(self.v_interp.values)
        for s_index in np.ndindex(self.v_interp.values.shape):
            
            s = tuple(self.v_interp.grid[i][s_index[i]] for i in range(0,len(s_index))) 
            
            v_new[s_index] = self._guess_v(s)
        self.v_interp.values = v_new
    
    def _init_A(self):
        
        self.A = dict()
        self.A_sum = dict()
        for tau in [2,3,4]:
            self.A[tau] = []
            prob_mult = 1
            for t in range(0,self.T):
                if (t%tau == 0) & (t != 0):
                    self.A[tau].append(prob_mult)
                    prob_mult = prob_mult*self.eta*self.beta**tau
                else:
                    self.A[tau].append(0)
            self.A[tau] = np.array(self.A[tau])
            
            self.A_sum[tau] = self.A[tau].sum() 
        
        self.B = dict()
        for tau in [2,3,4]:
            self.B[tau] = []
            prob_mult = 1
            for t in range(0,self.T):
                if (t%tau == 0):
                    self.B[tau].append(prob_mult)
                    prob_mult = prob_mult*self.eta*self.beta**tau
                else:
                    self.B[tau].append(0)
            self.B[tau] = np.array(self.B[tau])
        
    #################
    # Fitting the V #
    #################
    
    def _apply_v(self):
        v_combined = self.v_interp(self.points_evol_all)
        v_split_list = np.split(v_combined,len(self.s_all_list))
        v_split = {self.s_all_list[i]: v_split_list[i] for i in range(0,len(self.s_all_list))}
        return(v_split)
        
    def _apply_v_empirical_states(self):
        v_combined = self.v_interp(self.points_evol_empirical_states_all)
        v_split_list = np.split(v_combined,len(self.s_all_empirical_states_list))
        v_split = {self.s_all_empirical_states_list[i]: v_split_list[i] for i in range(0,len(self.s_all_empirical_states_list))}
        return(v_split)
        
    def _data_apply_v(self):
        v_combined = self.v_interp(self.data_points_evol_all)
        v_split_list = np.split(v_combined,len(self.data_s_all_list))
        v_split = {self.data_s_all_list[i]: v_split_list[i] for i in range(0,len(self.data_s_all_list))}
        return(v_split)
        
    def fit(self, detail = False):     
        eps_i = 1
        max_iter = 1000
        for i in range(0,max_iter):
            
            v_split = self._apply_v()      
            v_new = np.empty_like(self.v_interp.values)
  
            for s_index in np.ndindex(self.v_interp.values.shape):
                
                # Get the current state
                s = tuple(self.v_interp.grid[i][s_index[i]] for i in range(0,len(s_index))) 

                v_evol = v_split[s]
        
                esurp = np.array([self.surplus(s,
                                               x = {'tau': tau, 'value': self.v_dict[s][tau]},
                                               cost = self.cost_dict[s][tau], 
                                               v_evol = v_evol, 
                                               points_evol = self.points_evol[s] ) for tau in [2,3,4]])
                
                v_new[s_index] = max( (self.p_dict[s]*self.delta*esurp).sum() + self.beta*v_evol[1], 0 )
                
            eps_i = np.max( abs(v_new-self.v_interp.values) )
            s_max = np.argmax( abs(v_new-self.v_interp.values) )
            
            if (detail == True):
                print(eps_i,self.s_all_list[s_max])
                
            self.v_interp.values = v_new
            if (eps_i < 0.001): 
                break 
            
            assert (i < max_iter - 1), "Error: reached maximum number of iterations"
    
    def fit_v_use_price(self, detail = False):     
        eps_i = 1
        max_iter = 1000
        for i in range(0,max_iter):
            
            v_split = self._apply_v()      
            v_new = np.empty_like(self.v_interp.values)
            exp_pi = dict()
  
            for s_index in np.ndindex(self.v_interp.values.shape):
                
                # Get the current state
                s = tuple(self.v_interp.grid[i][s_index[i]] for i in range(0,len(s_index))) 

                v_evol = v_split[s]
                
                price_evol = self.dayrate_evol_dict[s]
        
                exp_pi = ( np.array([ self.expected_pi(x = {'tau': tau}, 
                                                       v_evol = v_evol, 
                                                       points_evol = self.points_evol[s], 
                                                       price_evol_tau = price_evol[tau] ) for tau in [2,3,4] ]) )
    
                v_new[s_index] = (self.p_dict[s]*exp_pi).sum() + (1-self.p_dict[s].sum())*(self.beta*v_evol[1])
                
                #print( (1-self.p_dict[s].sum()) )
                
            eps_i = np.max( abs(v_new-self.v_interp.values) )
            s_max = np.argmax( abs(v_new-self.v_interp.values) )
            
            if (detail == True):
                state_max = self.s_all_list[s_max]
                print(eps_i,state_max)
                             
            self.v_interp.values = v_new
            if (eps_i < 0.001): 
                break 
            
            assert (i < max_iter - 1), "Error: reached maximum number of iterations"
    
    def surplus(self, s, x = None, v_evol = None, points_evol = None, cost = None):
        if (points_evol is None):
            points_evol = self.state.evol(s,self.T)
        
        if (v_evol is None):
            v_evol = self.v_interp(points_evol)
            
        if (points_evol == 'use_empirical_states'):
            points_evol = self.points_evol_empirical_states[s]
        
        if (v_evol == 'use_empirical_states'):
            v_evol = self.v_evol_empirical_states[s]
        
        if (cost is None):
            cost = self.cost_total(x['mri'],x['tau'])
        
        surp = -self.beta*v_evol[1] 
        surp = surp + ( cost*self.A_sum[x['tau']]
                        + self.beta**x['tau']*self._value(x,points_evol)
                        + self.beta**x['tau']*(1-self.eta)*np.dot(self.A[x['tau']],v_evol) )
        
        return surp
    
    def expected_pi(self, x, v_evol, points_evol, price_evol_tau):
        
        '''
        if x['tau'] == 2:
            price_evol_tau = price_evol_tau*(1+self.beta)
            
        if x['tau'] == 3:
            price_evol_tau = price_evol_tau*(1+self.beta+self.beta**2)
            
        if x['tau'] == 4:
            price_evol_tau = price_evol_tau*(1+self.beta+self.beta**2+self.beta**3)
        '''
        
        
        result = (  np.dot(self.B[x['tau']],price_evol_tau)  
                    + self.beta**x['tau']*(1-self.eta)*np.dot(self.A[x['tau']],v_evol) )
        
        return result[0]
    
    ######################
    # Objects based on V #
    ######################                    
            
    def price(self, s, x, v_evol = None, points_evol = None, use_data = False, return_surplus_diff = False): 
        
        if use_data == False:
            if (points_evol is None):
                points_evol = self.state.evol(s,self.T + x['tau'])
            
            if (v_evol is None):
                v_evol = self.v_interp(points_evol)
            
        if use_data == True:
                points_evol = self.data_points_evol[s] 
                v_evol = self.data_v[s]
            
        surplus_0 = self.surplus(0,x,v_evol[:self.T],points_evol[:self.T]) 
        surplus_tau = self.surplus(0,x,v_evol[x['tau']:self.T+x['tau']],points_evol[x['tau']:self.T+x['tau'],:])
        
        continuation = (self.eta*(self.delta*surplus_tau + self.beta*v_evol[x['tau']+1]) 
                        + (1-self.eta)*v_evol[x['tau']] )
        
        price_i = ( self.delta*surplus_0 
                   + self.beta*v_evol[1] 
                   - self.beta**x['tau']*continuation ) 

        # PULL PRICE BACK TO PER PERIOD PRICE
        '''
        beta = self.beta
        
        if x['tau'] == 2:
            return price_i/(1+beta)
        
        if x['tau'] == 3:
            return price_i/(1+beta+beta**2)
        
        if x['tau'] == 4:
            return price_i/(1+beta+beta**2+beta**3)
        '''
        if return_surplus_diff:
            return surplus_0 - self.beta**x['tau'] * self.eta * surplus_tau

        return price_i
    
    def fit_price(self,data):
        
        
        # 1. Apply v
        self.data_v = self._data_apply_v()
        
        # 2. Fit prices
        prices_list = []
        for i in data.itertuples():
            prices_list.append( self.price(s = (i.g,i.n_l,i.n_m,i.n_h),
                                               x = {'tau': i.tau, 'mri': i.mri, 'value': i.value},
                                               use_data = True) )
        
        return np.array(prices_list)
        
    def fit_surplus_diff(self,data):
        
        
        # 1. Apply v
        self.data_v = self._data_apply_v()
        
        # 2. Fit prices
        surplus_diff_list = []
        for i in data.itertuples():
            surplus_diff_list.append( self.price(s = (i.g,i.n_l,i.n_m,i.n_h),
                                               x = {'tau': i.tau, 'mri': i.mri, 'value': i.value},
                                               use_data = True,
                                               return_surplus_diff = True) )
        
        return np.array(surplus_diff_list)
    
    def compute_residuals(self,data):
        
        p_hat = self.fit_price(data)
        
        p_hat = np.clip(p_hat,0,1000)
        
        plt.plot(data.index,p_hat,marker = '.', linestyle= '')
        plt.show()
        
        residuals = (data['dayrate'] - p_hat).dropna()

        return residuals
    
    ##########################
    # Plotting functionality #
    ##########################
    def plot_match(self):
        
        for i in ['mri','value']:
            s0, s1 = np.meshgrid(self.g_grid,self.n_grid)
            
            z_list = []
            for idx in itertools.product(range(len(self.g_grid)),range(len(self.n_grid))):
                s =  (s0[idx], s1[idx], 5, 5) 
                
                z = self.match['mean_' + i](s = s, tau = 3)
                z_list.append( z )
          
            z = np.array(z_list).reshape((len(self.g_grid),len(self.n_grid)))
            
            plt.contourf(s0,s1,z)
            plt.ylabel("n")
            plt.xlabel("g")
            plt.title(self.spec + ' | ' + i)
            plt.colorbar()
            plt.show()
    
    def plot_fitted_price(self,data,spec):
        data['p_hat'] = self.fit_price(data)
        data.to_stata('simulation_data_' + spec + '.dta')
        
        data.sort_values(['p_hat'],inplace = True)
        
        plt.plot(data['ym'],data['p_hat'],label='p_hat',ls = '', marker = '.')
        plt.plot(data['ym'],data['dayrate'],label='dayrate', ls = '', marker = '.')
        plt.title(self.spec)
        plt.legend()
        plt.show()
        
    def plot_value_function(self,data):
        data['v'] = self.v_interp(data)
        
        plt.plot(data['v'],label='value_function',ls = '', marker = '.')
        plt.title(self.spec)
        plt.legend()
        plt.show()
        
    def plot_fitted_price_2(self,data):
        data['p_hat'] = self.fit_price(data)
        
        plt.plot(data['p_hat'],label='p_hat',ls = '', marker = '.')
        plt.title(self.spec)
        plt.legend()
        plt.show()
 
class Surplus():
    '''
    This class makes the surplus attributes of V easier to access
    '''
    def __init__(self,model):
        self.model = model
    
    def surplus(self,s,x):
        
        surplus_dict = dict()
        for tau in [2,3,4]:
            surplus_list = []
            for spec in ['low','mid','high']:
                v_evol = self.model.v[spec].v_evol_empirical_states[s]
                points_evol = self.model.v[spec].points_evol_empirical_states[s]
                surplus_list.append( self.model.v[spec].surplus(s, x = {'tau': tau, 'mri': x}, v_evol = v_evol, points_evol = points_evol) )
            surplus_dict[tau] = np.array(surplus_list)
    
        return surplus_dict
    
    def cutoff(self,s,tau,spec):
        x0 = 0.75
        
        if spec == 'low':
            x_cutoff = scipy.optimize.root(lambda x: self.surplus(s,x)[tau][0],x0).x[0]
        
        if spec == 'mid':
            x_cutoff = scipy.optimize.root(lambda x: self.surplus(s,x)[tau][1],x0).x[0]
            if x_cutoff == x0:
                x_cutoff = 0
            
        if spec == 'high':
            x_cutoff = scipy.optimize.root(lambda x: self.surplus(s,x)[tau][2],x0).x[0]
        
        return x_cutoff
    
class Surplus_zero():
    '''
    This class makes the surplus attributes of V easier to access
    '''
    def __init__(self,model):
        self.model = model
    
    def surplus(self,s,x):
        
        surplus_dict = dict()
        for tau in [2,3,4]:
            surplus_list = []
            for spec in ['low','mid','high']:
                v_evol = self.model.v[spec].v_evol_empirical_states[s]
                v_evol = np.zeros_like(v_evol)
                points_evol = self.model.v[spec].points_evol_empirical_states[s]
                surplus_list.append( self.model.v[spec].surplus(s, x = {'tau': tau, 'mri': x}, v_evol = v_evol, points_evol = points_evol) )
            surplus_dict[tau] = np.array(surplus_list)
    
        return surplus_dict
    
    def cutoff(self,s,tau,spec):
        
        if spec == 'low':
            x_cutoff = 1.6
        
        if spec == 'mid':
            x_cutoff = 0
            
        if spec == 'high':
            x_cutoff = 0
        
        return x_cutoff
    
class Model():
    '''
    Interface for constructing and visualizing the model.
    '''
    def __init__(self,eta,params=None,estimate=False):   
            self.eta = eta
            self.beta = 0.99
        
            # Reading in data...
            print("Reading in the data")
            self._read_data()
            self.state_names = ['g','n_l','n_m','n_h']
            
            # Fitting the data...
            self._fit_match()
                
            if (estimate is True):
                self.estimate_v()
    
    def _read_data(self):
            self.data_state = pd.read_stata("state.dta")
            
            self.data_state = self.data_state[['ym',
                                               'gas_price',
                                               'gas_price_smooth',
                                               'n_available_spec_smooth_low',
                                               'n_available_spec_smooth_mid',
                                               'n_available_spec_smooth_high',
                                               'prob_match1',
                                               'prob_match2',
                                               'prob_match3',
                                               'n_available_spec1',
                                               'n_available_spec2',
                                               'n_available_spec3',]]
            
            for i, spec in enumerate( ['l','m','h'] ):
                self.data_state['u_' + spec] = ( 1- self.data_state['prob_match' + str(i+1)] ) * self.data_state['n_available_spec' + str(i+1)]  
            
            self.data_state.rename(columns = {'n_available_spec_smooth_low':'n_l',
                                         'n_available_spec_smooth_mid':'n_m',
                                         'n_available_spec_smooth_high':'n_h',
                                         'gas_price_smooth':'g'},inplace=True)
            
            self.data_state['ym'] = pd.to_datetime(self.data_state['ym'], unit = 'ms')
            
            self.data_state.set_index('ym',inplace = True)
    
            self.data_contracts = pd.read_csv("data_contracts.csv")
            
            self.data_contracts = self.data_contracts[['name_ihs',
                                                      'rig_spec',
                                                      'operator_ihs',
                                                      'contractor_ihs',
                                                      'contractstart',
                                                      'fixturedate',
                                                      'bid',
                                                      'dayrate',
                                                      'waterd',
                                                      'mri',
                                                      'duration',
                                                      'reneg',
                                                      'type']]
            
            self.data_contracts.rename(columns = {'bid': 'value'}, inplace = True)
                        
            self.data_contracts['value'] = self.data_contracts['value']/(1000000*5.41*30) #(1000000*5.41*30)
                        
            self.data_contracts['fixturedate'] = pd.to_datetime(self.data_contracts['fixturedate'],format = '%Y-%m-%d')
            
            self.data_contracts['contractstart'] = pd.to_datetime(self.data_contracts['contractstart'],format = '%Y-%m-%d')
            
            self.data_contracts = self.data_contracts[self.data_contracts['fixturedate'] > pd.to_datetime('2000-1-1',format='%Y-%m-%d') ]
            
            self.data_contracts['ym'] = self.data_contracts['fixturedate'].apply(lambda dt: dt.replace(day=1))
            
            self.data_contracts['mri'] = self.data_contracts['mri']/1000
            
            #self.data_contracts['mri'] = self.data_contracts['mri'].clip(lower = 0, upper = 1.6) 
            
            self.data_contracts['dayrate'] = self.data_contracts['dayrate']/1000000
            
            self.data_contracts['tau'] = pd.cut(self.data_contracts['duration'],bins=[0,75,105,10000],labels=[2,3,4])
            
            self.data_contracts['dayrate_total'] = np.nan
            
            self.data_contracts.loc[self.data_contracts['tau'] == 2, 'dayrate_total'] = self.data_contracts['dayrate'] * (1 + self.beta)
            self.data_contracts.loc[self.data_contracts['tau'] == 3, 'dayrate_total'] = self.data_contracts['dayrate'] * (1 + self.beta + self.beta**2)
            self.data_contracts.loc[self.data_contracts['tau'] == 4, 'dayrate_total'] = self.data_contracts['dayrate'] * (1 + self.beta + self.beta**2 + self.beta**3)
            
           # self.data_contracts['value'] = self.data_contracts['value'].clip(lower = 0.025, upper = 0.033)
            
            self.data_contracts_state = self.data_contracts.merge(self.data_state, 
                                                                  left_on = 'ym', 
                                                                  right_index = True, 
                                                                  how = 'left')
            
            pd.DataFrame.to_stata(self.data_contracts_state,fname = 'contracts_final.dta')
            # Create data with different length contracts
                        
            data_tau0 = [self.data_contracts[['ym','tau','rig_spec']]]
            for s, spec in zip(['l','m','h'],['low','mid','high']):
                data_tau0.append(pd.DataFrame(data = [[0, spec + '-spec']], 
                                              index = self.data_state.index.repeat(self.data_state['u_' + s]), 
                                              columns = ['tau','rig_spec'] ).reset_index())
            
            self.data_mnl = pd.concat(data_tau0).reset_index(drop=True)

    ####################################
    # Do regressions for mri and value #
    ####################################
    
    def _fit_match(self):
        
        # 1 . Get covariates 
        
        poly = PolynomialFeatures(2, include_bias = False)     
        poly_fit = poly.fit_transform(self.data_state[self.state_names])
        poly_names = poly.get_feature_names(self.state_names)
        
        data_state_poly = pd.DataFrame(poly_fit,
                                       columns = poly_names,
                                       index = self.data_state.index.values)
        
        data_contracts_poly = self.data_contracts.merge(data_state_poly, 
                                                        left_on = 'ym', 
                                                        right_index = True, 
                                                        how = 'left')
        
        data_mnl_poly = self.data_mnl.merge(data_state_poly, 
                                            left_on = 'ym', 
                                            right_index = True, 
                                            how = 'left')
        
        # 2. Do regressions and package as `match_all' for simulation 
        
        reg = dict()   
        data_contracts = dict()
        self.match_all = {'low-spec': dict(), 'mid-spec': dict(), 'high-spec': dict()}
        
        for i, j in itertools.product(['tau'],['low-spec','mid-spec','high-spec']):
            
            def reg_fn_1(i,j):
                
                data_contracts[i + ' ' + j] = data_mnl_poly[(data_mnl_poly['rig_spec'] == j)].dropna(subset=[i])
                
                reg[i + ' ' + j] = LogisticRegression(random_state=0, multi_class='multinomial', solver = 'newton-cg', max_iter = 10000).fit(data_contracts[i + ' ' + j][poly_names],
                                                                                                                                             data_contracts[i + ' ' + j][i])
                
                # Feed in s = [g,n_l,n_m,n_h]
                # Only returns [2,3,4] probabilities...
                
                def fn_1(s):
                    return reg[i + ' ' + j].predict_proba( poly.fit_transform(np.array(s).reshape(1,-1)) )[:,1:4][0]
                
                return fn_1
            
            self.match_all[j][i] = reg_fn_1(i,j)
            
            ################
            #DELETE BELOW: #
            ################
            #self.match_all[j]['mean_' + i] = lambda s: 0.7

        for i, j in itertools.product(['mri','value','dayrate'],['low-spec','mid-spec','high-spec']):
            #NOTE I DROP NAN
            
            def reg_fn_2(i,j):
                # Feed in s = [g,n_l,n_m,n_h]
                data_contracts[i + ' ' + j] = data_contracts_poly[(data_contracts_poly['rig_spec'] == j)].dropna(subset=[i])

                reg[i + ' ' + j] = LinearRegression().fit(data_contracts[i + ' ' + j][poly_names + ['tau'] ],
                                                          data_contracts[i + ' ' + j][i] )
                def fn_2(s,tau):
                    
                    X = np.append(poly.fit_transform(np.array(s).reshape(1,-1)), np.array([[tau]]), axis = 1 )
                    prediction = reg[i + ' ' + j].predict( X )
                    
                    # ENSURE IT IS > 0!
                    prediction = prediction.clip(min = 0)
                    return prediction
                
                return fn_2
            
            self.match_all[j]['mean_' + i] = reg_fn_2(i,j)

            ################
            #DELETE BELOW: #
            ################
            #if (i == 'value'):
             #   self.match_all[j]['mean_' + i] = lambda s, tau: 0.033
                
            #if (i == 'mri'):
             #   self.match_all[j]['mean_' + i] = lambda s, tau: 0.8

    def plot_fit_match(self):
        for i in ['value','mri','dayrate']:
            for tau in [2,3,4]:
                for spec in ['low-spec','mid-spec','high-spec']:
                    y = self.data_state[self.state_names].apply(lambda s: self.match_all[spec]['mean_' + i](s,tau), axis = 1)
                    plt.plot(y, label = spec)
                plt.legend()
                plt.title('Type: ' + i + ' Tau: ' + str(tau))
                plt.show()
                
        for i in [0,1,2]:        
            for spec in ['low-spec','mid-spec','high-spec']:
                y = self.data_state[self.state_names].apply(lambda s: self.match_all[spec]['tau'](s)[i], axis = 1)
                plt.plot(y, label = spec)
            plt.legend()
            plt.title('Type: prob. match' + ' Tau: ' + str(i+2))
            plt.show()
            
    def plot_contour_match(self,variables = None):
        
        self.g_grid = np.linspace(2,10,5)
        self.n_grid = np.linspace(10,25,5)
        
        if variables == None:
            variables = ['mri','value','dayrate']
        
        for spec in ['low-spec','mid-spec','high-spec']:
            for i in variables:
                s0, s1 = np.meshgrid(self.g_grid,self.n_grid)
                
                z_list = []
                for idx in itertools.product(range(len(self.g_grid)),range(len(self.n_grid))):
                    s =  (s0[idx], s1[idx], 5, 5) 
                    
                    z = self.match_all[spec]['mean_' + i](s = s, tau = 3)
                    z_list.append( z )
              
                z = np.array(z_list).reshape((len(self.g_grid),len(self.n_grid)))
                
                plt.contourf(s0,s1,z)
                plt.ylabel("n")
                plt.xlabel("g")
                plt.title(spec + ' | ' + i)
                plt.colorbar()
                plt.show()
    
    
    ######################
    # Fit data to params #
    ######################
    
    def compute_residuals_wrapper(self,x,x_names,spec_use,data,verbose = False, use_ss = False):
        params = x_names(x)
        
        if verbose == True:
            print(params)
        
        # Add in new variables and fit     
        residuals_list = []
        
        for spec in spec_use:
            self.v[spec].delta = params.loc['delta']
            self.v[spec].c = params.loc[spec]
            
            if use_ss == False:
                self.v[spec].fit()
            
            if use_ss == True:
                self.v[spec].fit_guess_v()
            
            residuals_list.append( self.v[spec].compute_residuals(data[data['rig_spec'] == spec]) )
        
        residuals = pd.concat(residuals_list)
        
        return residuals
    
    def gen_x_names(self,x_init,x_bounds):   
        # Generates a function that produces names of x from a given init.
        
        spec_use = []
        for spec in ['low','mid','high']:
            if spec + '-spec' in x_init:
                spec_use.append(spec + '-spec')

        def x_names(x):
            #######!
            # CHANGE BELOW!!!!!!!!!!
            ####################!
            x_names_dict = {'delta': x[0]}
            
            k = 1
            for spec in spec_use:
                x_names_dict = {**x_names_dict, **{spec: np.array([x[k],x[k+1]])} } 
                k += 2
                
            x_series = pd.Series(x_names_dict)
            return x_series
        
        # Flatten out the inital input
        x_init_flatten = [x_init['delta']]
        x_bounds_flatten = ([x_bounds['delta'][0]],[x_bounds['delta'][1]])
        
        for spec in spec_use:
            x_init_flatten.append( x_init[spec][0] )
            x_init_flatten.append( x_init[spec][1] )
            
            for i in range(2):
                x_bounds_flatten[i].append( x_bounds[spec][0][i] )
                x_bounds_flatten[i].append( x_bounds[spec][1][i] )
        
        return x_names, spec_use, x_init_flatten, x_bounds_flatten
    
    def std_errors(self,J,x_names):
        
        cov = np.linalg.inv(J.T.dot(J))
        var = np.sqrt(np.diagonal(cov))
        
        print("STD ERROR")
        print(x_names(var))
        
    def estimate_v(self, x_init = None, x_bounds = None, verbose = False, use_ss = False, plot = False):
        # Define a default inital guesses
        
        if x_init is None:
            x_init = pd.Series({'delta': 0.27805,
                                'low-spec': np.array([0,-0.04]),
                                'mid-spec': np.array([-0.01,-0.03]),
                                'high-spec': np.array([-0.01,-0.01]),
                                })
    
        if x_bounds == 'negative':
            x_bounds = pd.Series({'delta': (0,1),
                                  'low-spec': np.array([(-np.inf,0),(-np.inf,0)]),
                                  'mid-spec': np.array([(-np.inf,0),(-np.inf,0)]),
                                  'high-spec': np.array([(-np.inf,0),(-np.inf,0)]),
                                  })
    
        if x_bounds is None:
            x_bounds = pd.Series({'delta': (0.278,0.2781),
                                  'low-spec': np.array([(-np.inf,np.inf),(-np.inf,np.inf)]),
                                  'mid-spec': np.array([(-np.inf,np.inf),(-np.inf,np.inf)]),
                                  'high-spec': np.array([(-np.inf,np.inf),(-np.inf,np.inf)]),
                                  })
            
        x_names, spec_use, x_init_flatten, x_bounds_flatten = self.gen_x_names(x_init,x_bounds)
        
        data = self.data_contracts_state
        
        print('Setting up the inital v')
        self.v = dict()
        for spec in spec_use:
            print('Doing: ' + spec)
            self.v[spec] = V( match = self.match_all[spec], 
                              eta = self.eta, 
                              delta = x_init['delta'], 
                              c = x_init[spec],  
                              spec = spec, 
                              data = self.data_state) 
        
        #UNCOMMENT BELOW!
        results = scipy.optimize.least_squares( self.compute_residuals_wrapper, 
                                               x0 = x_init_flatten, 
                                               bounds = x_bounds_flatten,
                                               #xtol = 0.00001,
                                               #max_nfev = 1,
                                               args = (x_names, spec_use, data, verbose, use_ss), 
                                               verbose = True ) 

        self.std_errors(results.jac,x_names)
        #DELETE BELOW!!
        #x_optimal = x_init
        
        if plot == True:
            #x_optimal = x_names(results['x'])
            for spec in spec_use:
                #self.v[spec] = V(match = self.match_all[spec], eta = self.eta, delta = x_optimal['delta'], c = x_optimal[spec],  spec = spec) 
                self.v[spec].plot_fitted_price(data[data['rig_spec'] == spec])
    
    def fit_v(self,params,spec_use = ['low','mid','high'],fit=True):
     
        self.v = dict()
        for spec in spec_use:
            print('Doing: ' + spec)
            self.v[spec] = V( match = self.match_all[spec + '-spec'], 
                              eta = self.eta, 
                              delta = params['delta'], 
                              c = [params[spec + '0'],
                                   params[spec + '1'],
                                   params['gas0'],
                                   params['gas1']],  
                              spec = spec, 
                              data = self.data_state,
                              fit = fit,
                              use_empirical_states = True) 
    
    def fit_match_params(self,match_params,spec_use = ['low','mid','high']):
        '''
        Just fit the params (for use with price estimation of V)
        '''
        for spec in spec_use:
            self.v[spec].c = [match_params[spec + '0'], match_params[spec + '1'],match_params['gas0'],match_params['gas1']]
            self.v[spec].delta = match_params['delta']
            
        # GET EMPIRICAL STATE V:
        
    
    ###############
    # Using LMFit #
    ###############
    def compute_residuals_wrapper_lmfit(self,params,spec_use,data):
        #print(params.valuesdict())
        
        # Add in new variables and fit     
        residuals_list = []
        
        for spec in spec_use:
            self.v[spec].delta = params['delta'].value
            self.v[spec].c = [ params[spec + '0'],
                               params[spec + '1'],
                               params['gas0'],
                               params['gas1']]
            
            residuals_spec = self.v[spec].compute_residuals(data[data['rig_spec'] == spec+'-spec'])
            
            residuals_list.append( residuals_spec )
        
        residuals = pd.concat(residuals_list)
        
        print(len(residuals))
        
        return residuals

    def estimate_v_lmfit(self, x_bounds = None, verbose = False, plot = False, use_saved_model = False): 
        
        ########################
        # Setup the parameters #
        ########################
        params = lmfit.Parameters()
        
        params.add('delta', value = 0.5, min = 0, max = 1)
        #params.add('intercept', value = -0.01, min = -1, max = 1)
        params.add('low0', value = 0, min = -1, max = 1)
        params.add('low1', value = -0.02, min = -1, max = 1)
        params.add('mid0', value = 0, min = -1, max = 1)
        params.add('mid1', value = -0.02, min = -1, max = 1)
        params.add('high0', value = 0, min = -1, max = 1)
        params.add('high1', value = -0.02, min = -1, max = 1)
        params.add('gas0', value = 0.1, min = -1, max = 1)
        params.add('gas1', vary = True, value = 0.1, min = -1, max = 1)

        #spec_use = list(set([i[:-1] for i in list(params.keys()) if (i != 'delta') & (i != 'intercept') ]) ) 

        spec_use = ['low','mid','high']

        data = self.data_contracts_state
        # CHANGE BELOW
        #data['tau'] = 3
        
        if (use_saved_model == False):
            print('Setting up the inital v')
            self.v = dict()
            for spec in spec_use:
                print('Doing: ' + spec)
                self.v[spec] = V( match = self.match_all[spec + '-spec'], 
                                  eta = self.eta, 
                                  delta = params['delta'].value, 
                                  c = [params[spec + '0'].value,
                                       params[spec + '1'].value,
                                       params['gas0'].value,
                                       params['gas1'].value],
                                  spec = spec, 
                                  data = self.data_state) 
                
            print('Saving v')
            f = open('model_dill_' + spec, 'wb') 
            dill.dump(self.v[spec],f)
            f.close()
            
        if (use_saved_model == True):
            for spec in spec_use:
                print("SAVED")
                print('Loading saved v')
                f = open('model_dill_' + spec, 'rb')
                self.v[spec] = dill.load(f)
                f.close()
            
        #######################
        # Do the minimization #
        #######################
        
        fitter = lmfit.Minimizer( userfcn = self.compute_residuals_wrapper_lmfit, 
                                  params = params, 
                                  fcn_args = (spec_use, data) ) 

        all_methods = ['leastsq',
                       #'least_squares',
                       #'differential_evolution',
                       #'brute',
                       #'basinhopping',
                       #'ampgo',
                       #'nelder',
                       #'lbfgsb',
                       #'powell',
                       #'cg',
                       #'cobyla',
                       #'bfgs',
                       #'tnc',
                       #'trust-ncg',
                       #'trust-exact',
                       #'trust-krylov',
                       #'trust-constr',
                       #'dogleg',
                       #'slsqp',
                       #'emcee',
                       ]
        
        for method in all_methods:
            print("METHOD: " + method)
            results = fitter.minimize(method = method)   
            print(lmfit.fit_report(results))
           
        
        if plot == True:
            #data['value'] = 0 
            for spec in spec_use:
                data.loc[data['rig_spec'] == spec + '-spec','dayrate']
                self.v[spec].plot_fitted_price(data.loc[data['rig_spec'] == spec + '-spec'],spec)


#model = Model(eta=0.5)
#model.estimate_v_lmfit(plot = False)



