import numpy as np
from scipy.interpolate import RegularGridInterpolator
def f(x,y):
    return 2 * x**3 + 3 * y**2 
x = np.linspace(1, 4, 11)
y = np.linspace(4, 7, 22)
data = f(*np.meshgrid(x, y, indexing='ij', sparse=True))

print(data.round(0))
print(data.shape)