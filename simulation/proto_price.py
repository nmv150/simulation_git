import value_functions
import numpy as np

def test_price():
    match = {'tau': lambda s: np.array([0,s[0]/15,0]),
            'mean_value': lambda s, tau: 0,
            'mean_mri': lambda s, tau: 1
            }
    eta = 0.2
    delta = 0.5
    c = [1,0]

    V = value_functions.V(match,eta)
    
    V.fit(c,delta)
    V.plot(plot_price = False)

def v_soln(m,p,delta,beta,u_c):
    return ((p*delta*m*(1+beta+beta**2)-u_c)/((1-beta)+p*delta*beta*(1-beta**2))).round(3)

def v_soln_no3(m,p,delta,beta,u_c):
    return ((p*delta*m*(1+beta+beta**2)-u_c)/((1-beta)+p*delta*beta)).round(3)

def price(m,p,delta,beta,u_c):
    V = v_soln(m,p,delta,beta,u_c)
    return ((delta*m*(1+beta+beta**2) + (1-delta)*(beta*V-u_c-beta**3*V))/(1+beta+beta**2)).round(3)

def price_no3(m,p,delta,beta,u_c):
    V = v_soln_no3(m,p,delta,beta,u_c)
    return ((delta*m*(1+beta+beta**2) + (1-delta)*(beta*V-u_c))/(1+beta+beta**2)).round(3)

m = 0.3
u_c = 0 
delta = 0.15
p_i = np.array([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])

print(list(zip(v_soln(m,p_i,delta,0.99,u_c),p_i)))
print(list(zip(price(m,p_i,delta,0.99,u_c),p_i)))

#print(list(zip(v_soln(m,p_i,delta,0.99,u_c),v_soln_no3(m,p_i,delta,0.99,u_c))))
#print(list(zip(price(m,p_i,delta,0.99,u_c),price_no3(m,p_i,delta,0.99,u_c))))